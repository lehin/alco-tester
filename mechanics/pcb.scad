delta=1e-3 ;
mil = 25.4/1000 ;

function mil2mm (x) = x * mil ;
function mm2mil (x) = x / mil ;

pcb_thick = 1.5 ;

module top (off = [0, 0])
{
   translate (off)
      rotate ([180, 0, 0])
         children() ;
}

module bottom (off = [0, 0])
{
   translate ([off[0], off[1], mm2mil (pcb_thick)])
      children() ;
}

module terminal5 (cnt)
{
   scale (1/mil)
      for (n = [ 0 : cnt - 1])
         translate ([n * 5, 0, 0])
         {
            color ([0.4, 0.4, 0.8])
               difference()
               {
                  hull()
                  {
                     cube ([5, 7.5, 6.9]) ;
                  
                     translate ([0, 1.25, 9])
                        cube ([5, 5, 1]) ;
                  }
                  
                  translate ([2.5, 3.75, 8])
                     cylinder (d = 3.5, h = 2 + delta) ;
               }

            color([0.9,0.9,0.9])
               translate ([2.5, 3.75, 8])
                  cylinder (d = 3.5, h = 1) ;
         }
}

module terminal35 (cnt)
{
   scale (1/mil)
      for (n = [ 0 : cnt - 1])
         translate ([n * 3.5, 0, 0])
         {
            color ([0.75, 0.9, 0.4])
               difference()
               {
                  hull()
                  {
                     cube ([3.5, 7, 5]) ;
                  
                     translate ([0, 1.5, 7.7])
                        cube ([3.5, 4, 1]) ;
                  }
                  
                  translate ([1.75, 3.5, 6.7])
                     cylinder (d = 2.5, h = 2 + delta) ;
               }

            color([0.9,0.9,0.9])
               translate ([1.75, 3.5, 6.7])
                  cylinder (d = 2.5, h = 1) ;
         }
}

module pld (cnt, rows)
{
   for (r = [ 0 : rows - 1])
      for (n = [ 0 : cnt - 1])
         translate([n * 100, r * 100, 0])
         {
            color ("black")
               translate ([-100 / 2, -100 / 2, 0])
                  cube (100, 100, 80) ;

            translate ([-25 / 2, -25 / 2, 0])
               color ([1, 1, 0.6])
                  cube ([25, 25, 315]) ;
         }
}

module led (offset = 50)
{
   color ("white")
      translate ([0,0,offset])
      {
         H = 177 + 31 - 118/2 ;
         cylinder (d = 118, h = H) ;
         cylinder (d = 154, h = 31) ;
         translate ([0, 0, H - delta])
            sphere (d = 118) ;
      }
      
   color ([0.9, 0.9, 0.9])
   {
      translate ([-50-10, -10, 0])
         cube ([20, 20, offset]) ;

      translate ([50-10, -10, 0])
         cube ([20, 20, offset]) ;
   }
}

module photoresistor (offset = 100)
{
   color ([0.9, 0.7, 0.7])
      translate ([0, 0, offset])
      {
         intersection()
         {
            D = mm2mil (5.1) ;
            H = mm2mil (1.8) ;
            C = mm2mil (4.3) ;
            
            cylinder (d = D, h = H) ;
            
            translate ([-D/2, -C/2, 0])
               cube ([D, C, H]) ;
         }
      }

   P = mm2mil (3.4) ;

   color ([0.9, 0.9, 0.9])
   {
      translate ([-P/2, 0, 0])
         cylinder (d = mm2mil (0.5), h = offset) ;

      translate ([P/2, 0, 0])
         cylinder (d = mm2mil (0.5), h = offset) ;
   }
}

module fresnel()
{
   B = mm2mil(23) ;
   Bh = mm2mil (3) ;
   H = mm2mil (15) ;
   
   color ([0.9, 0.9, 0.9])
   {
      translate ([-B/2, -B/2, 0])
         cube ([B, B, Bh]) ;

      hull()
      {
         cylinder (d = B, h = Bh) ;

         translate ([0, 0, H - B/2])
            intersection()
            {
               sphere (d = B, $fn = 15) ;

               translate ([-B/2, -B/2, 0])
                  cube ([B, B, B/2]) ;
            }
      }
   }
}

module esp07()
{
   size = [865, 630, mm2mil(1)] ;
   shield = mm2mil ([15, 12, 2.5]) ;
   
   translate ([-size[0] / 2, -size[1] / 2, 0])
   {
      color ([0, 0.2, 0.5])
         cube (size) ;
      
      color ([0.9, 0.9, 0.9])
         translate ([size[0] - shield[0] - mm2mil (1.2), (size[1] - shield[1]) / 2, size[2]])
            cube (shield) ;
   }
}

module bh10r()
{
   cnt = 5 ;
   h = mm2mil([6,6]) - [0, 100] ;
   
   for (r = [0 : 1])
      for (n = [ 0 : cnt - 1])
         translate([n * 100, r * 100, 0])
         {
            translate ([-25 / 2, -25 / 2, 0])
               color ([1, 1, 0.6])
                  cube ([25, 25, h[r]]) ;
         }

   for (r = [0 : 1])
      for (n = [ 0 : cnt - 1])
         translate([n * 100, r * 100, h[r] - 25/2])
         {
            rotate ([-90, 0, 0])
               translate ([-25 / 2, -25 / 2, 0])
                  color ([1, 1, 0.6])
                     cube ([25, 25, mm2mil(13) - r * 100]) ;
         }
         
   case = mm2mil ([20.35, 8.85, 9]) ;
   
   color ([0.3, 0.3, 0.3])      
      translate ([-(case[0] - 400) / 2, mm2mil (4.5), 0])
         difference()
         {
            cube (case) ;
            
            th = mm2mil (0.9) ;
            translate ([th, th, th])
               cube (case - [th*2, 0, th*2]) ;
         }
}

module cdrh5d28r()
{
   sz = mm2mil ([6.2, 6.3, 3.0]) ;
   
   color ([0.3, 0.3, 0.3])
      render()
         intersection()
         {
            translate ([-sz[0]/2, -sz[1]/2, 0])
               cube (sz) ;
         
            sz2 = [sz[0] * 2, mm2mil (7.2), sz[2]] ;
            rotate ([0,0,-45])
               translate ([-sz2[0]/2, -sz2[1]/2, 0])
                  cube (sz2) ;
         }
}

module usb_5s_b()
{
   sz = mm2mil ([7.80, 5.00 + 0.65, 2.80]) ;
   base = [sz[0] / 2, mm2mil (2.15 + 0.65), (sz[2] - mm2mil (2.35)) / 2] ;

   color ([0.7, 0.7, 0.7])
      translate (-base)
         cube (sz) ;
}

module msk12c02 (pos = false)
{
   sz = mm2mil ([6.7, 2.7, 1.4]) ;
   base = [sz[0] / 2, sz[1]/2, 0] ;

   translate (-base)
      color ([0.7, 0.7, 0.7])
         cube (sz) ;
   
   handle = mm2mil ([1.3, 1.5, 1.1]) ;
   
   translate ([mm2mil (pos ? -1.5 : 1.5) / 2 - handle[0] / 2, sz[1]/2, mm2mil (0.1)])
      color ([1, 1, 1])
         cube (handle) ;
}

module fuse_fh102()
{
   scale (1/mil)
      translate ([-22/2, -10.5/2])
      {
         color ([0.9, 0.9, 0.7])
            cube ([22, 10.5, 5.5]) ;
         
         color ([0.8, 0.8, 0.8])
         {
            translate ([0, 1.5, 5.5])
               cube ([5, 7.5, 14.4-5.5]) ;

            translate ([22 - 5, 1.5, 5.5])
               cube ([5, 7.5, 14.4-5.5]) ;
         }

         color ([1,1,1])
            translate ([1, 10.5 / 2, 14 - 6.5/2])
               rotate ([0, 90, 0])
                  cylinder (d = 6.5, h = 20) ;
      }
}

module varistor_s05kx()
{
   scale (1/mil)
   {
      color ([0.9, 0.7, 0.3])
         translate ([0, 2.5, 3.5])
            rotate ([90, 0, 0])
               cylinder (d = 5, h = 4) ;
   }
}

module hlk_pm01()
{
   sz = [34, 20.2, 15] ;
   
   scale (1/mil)
      color ([0.5, 0.5, 0.9])
         translate ([-(sz[0] - 29.4)/2, -(sz[1]/2 + 2.5)])
            cube (sz) ;
}

module hcm1206x()
{
   scale (1/mil)
      color ([0.3, 0.3, 0.3])
         cylinder (d = 12, h = 9.5) ;
}

module mq135()
{
   color ([0.9, 0.9, 0.9])
      scale (1/mil)
      {
         cylinder (d = 19.5, h = 5.3) ;

         hull()
         {
            translate ([0, 0, 5.3-delta])
               cylinder (d = 16, h = delta) ;

            translate ([0, 0, 14.6])
               cylinder (d = 13, h = delta) ;
         }
      }
}

module mq3()
{
   color ([0.9, 0.6, 0])
      scale (1/mil)
         cylinder (d = 16.5, h = 9) ;
}

module wl102_transmitter()
{
   scale (1/mil)
      translate ([0, -5.2, 0])
      {
         translate ([-(17 - 2.54*3)/2, 0, 0])
         {
            color ([0, 0.7, 0])
               cube ([17, 1.2, 13]) ;
            
            color ([0.9, 0.9, 0.9])
               translate ([17 + 2.5, 1.2 + 4.5/2, 11.5 + 4.5/2])
                  rotate ([0, 90, 0])
                     cylinder (d = 4.5, h = 14) ;
         }
         
         color ([0.2, 0.2, 0.2])
            translate ([-2.54/2, 1.2])
               cube ([2.54 * 4, 2.5, 2.5]) ;
         
         color ([0.8, 0.8, 0.8])
            for (i = [0:3])
               translate ([i * 2.54, 2.5 + 1.2, 2.5/2])
               {
                  translate ([-0.3, 0, -0.3])
                     cube ([0.6, 5.5 - 2.5 - 1.2, 0.6]) ;

                  translate ([-0.3, 5.2 - 2.5 - 1.2 - 0.3, -2.5/2])
                     cube ([0.6, 0.6, 2.5/2+0.3]) ;
               }
      }
}

module led0805()
{
   scale (1/mil)
      translate ([-2/2, -1.25/2])
         cube ([2.0, 1.25, 1]) ;
}

module hps16()
{
   color ([0.6, 0.6, 0.6])
      scale (1/mil)
         translate ([-8, -8])
            cube ([16, 16, 3]) ;
}

module cdrh104r()
{
   color ([0.4, 0.4, 0.4])
      scale (1/mil)
         translate ([-10.5/2, -10.5/2])
            cube ([10.5, 10.5, 4]) ;
}

module oled091()
{
   scale (1/mil)
   {
      color ([0.8, 0.8, 0.8])
         cube ([26.6, 11.5, 1.2]) ;
 
      color ([0.3, 0.3, 0.3])
         translate ([2.1, 2.1])
            cube ([22.384, 5.584, 1.2 + delta]) ;
  }
}

module btn2x4r()
{
   scale (1/mil)
   {
      translate ([-4.4/2, -1.1])
         color ([1, 1, 0.7])
            cube ([4.4, 2, 1.8]) ;

      translate ([-1.9/2, 0.9])
         color ([0, 0, 0])
            cube ([1.9, 1.2, 1]) ;
  }
}

module xsm127x2r()
{
   scale (1/mil)
   {
      translate ([-4.8/2, 3.5])
         color ([0.3, 0.3, 0.3])
            cube ([4.8, 2.6, 3]) ;
  }
}