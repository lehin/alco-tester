 include <pcb.scad>

$fn = 30 ;
stdgap = 0.2 ;

pcb_size = [ 1650, 1500, mm2mil (pcb_thick) ] ;
pcb_size_mm = [ for (i = pcb_size) mil2mm (i) ] ;
   
thick = 2.5 ;
pcbgap = 0.5 ;
pcb_z = 3;
battery_sz = [77, 21, 21.5] + [4*stdgap, 2*stdgap, 2*stdgap] ;
device_height = battery_sz[2] + stdgap ;
battery_offs = [41, 2] ;
case_sz = 
[
   battery_offs[0] + battery_sz[1] + thick*2,
   battery_sz[0] + battery_offs[1] * 2 + thick*2, 
   device_height + 2 * thick
] ;

cap_sz = 
[
   case_sz[0] - 2*thick + 2,
   case_sz[1] - 2*thick + 2,
   thick
] ;

sens_offs_x = 28.8 ;

module place_pcb (Z = pcb_z + thick)
{
   translate ([thick + pcbgap, thick + pcbgap, Z])
      children() ;
}

module place_battery (Z = thick)
{
   translate ([battery_offs[0] + battery_sz[1] + thick, battery_offs[1] + thick, Z])
      children() ;
}

module rcube (dim = [1,1,1], rdim = -1, rdims = [0.1, 0.1, 0.1, 0.1])
{
   module impl (rdims)
   {
      hull()
      {
         if (rdims[0] > 0)
            translate ([rdims[0], rdims[0], 0]) cylinder (r=rdims[0], h=dim[2]) ;
         else
            cube ([delta, delta, dim[2]]) ;

         translate ([dim[0], 0, 0])
            if (rdims[1] > 0)
               translate ([-rdims[1], rdims[1], 0]) cylinder (r=rdims[1], h=dim[2]) ;
            else
               translate ([-delta, 0, 0])
                  cube ([delta, delta, dim[2]]) ;

         translate ([dim[0], dim[1], 0])
            if (rdims[2] > 0)
               translate ([-rdims[2], -rdims[2], 0]) cylinder (r=rdims[2], h=dim[2]) ;
            else
               translate ([-delta, -delta, 0])
                  cube ([delta, delta, dim[2]]) ;

         translate ([0, dim[1], 0])
            if (rdims[3] > 0)
               translate ([rdims[3], -rdims[3], 0]) cylinder (r=rdims[3], h=dim[2]) ;
            else
               translate ([0, -delta, 0])
                  cube ([delta, delta, dim[2]]) ;
      } ;
   }
   
   if (rdim >= 0)
      impl ([rdim, rdim, rdim, rdim]) ;
   else
      impl (rdims) ;
}

module pcb_mil()
{
   delta = mm2mil (delta) ;

   color ([0.8, 0.8, 0.2])
      cube (pcb_size) ;
   
   bottom ([425, 1075])
      mq3() ;

   bottom ([447, 325])
      hps16() ;
   
   bottom ([1200, 550])
      cdrh104r() ;
   
   bottom ([1577, 1251])
      rotate ([0,0,90])
      usb_5s_b() ;
   
   top ([1650 - mm2mil (11.5), 1500 - mm2mil (26.6)])
      rotate ([0, 0, -90])
         oled091() ;

   top ([760, 25])
      btn2x4r() ;
      
   top ([247.5, 125])
      rotate ([0, 0, 90])
         xsm127x2r() ;
}

module pcb()
{
   translate ([0, pcb_size_mm[0], 0])
      rotate ([0, 0, -90])
         scale (mil)
            pcb_mil() ;
}

module bat18650()
{
   rotate ([0, 0, 90])
      color ([0.3, 0.3, 0.3])
         cube (battery_sz) ;
}

module click (L)
{
   translate ([0, -L/2, 1])
      rotate ([-90, 0, 0])
         linear_extrude (height = L)
            polygon ([[0,0], [0.6,0.7], [0, 1]]) ;
}

module case_base()
{
   difference()
   {
      rcube (case_sz, rdim = 2) ;
      
      translate ([thick, thick, thick])
         cube (case_sz - [thick*2, thick*2, 0]) ;
   }

   translate ([battery_offs[0] + thick, thick - delta, thick - delta])
      cube ([battery_sz[1] + delta, battery_offs[1] + delta, 6.5]) ;

   translate ([battery_offs[0] + thick - 0.5, case_sz[1] - thick - battery_offs[1] + delta, thick - delta])
      cube ([battery_sz[1] + 0.5 + delta, battery_offs[1] + delta, 6.5]) ;

   translate ([battery_offs[0], 55, thick - delta])
      cube ([thick, case_sz[1] - 55 - thick + delta, 6.5]) ;
   
   translate ([thick + 2*pcbgap + pcb_size_mm[1], thick - delta, thick - delta])
      cube ([1, pcb_size_mm[0] + 2*pcbgap + 1, pcb_z + pcb_size_mm[2] + 1]) ;

   translate ([thick + 7, thick + 2*pcbgap + pcb_size_mm[0], thick - delta])
      cube ([pcb_size_mm[1] + 2*pcbgap + 1 - 7, 1, pcb_z + pcb_size_mm[2] + 1]) ;
   
   translate ([thick - delta, thick + 7, thick - delta])
      cube ([2, 10, pcb_z]) ;

   translate ([thick + 7, thick + 2*pcbgap + pcb_size_mm[0] + delta - 2, thick - delta])
      cube ([pcb_size_mm[1] + 2*pcbgap - 7.5, 1.5, pcb_z]) ;

   translate ([thick + 2*pcbgap + pcb_size_mm[1] - 2, thick + 13, thick - delta])
      cube ([1.5, pcb_size_mm[0] + 2*pcbgap - 13.5, pcb_z]) ;
      
   translate ([thick - delta, thick + 12, thick + pcb_z + pcb_size_mm[2]])
      click (L = 10) ;

   translate ([thick + pcb_size_mm[1] + 2*pcbgap + delta, thick + (pcb_size_mm[0] + 2*pcbgap) / 2, thick + pcb_z + pcb_size_mm[2]])
      rotate ([0, 0, 180])
         click (L = 15) ;

   translate ([thick + (pcb_size_mm[1] + 2*pcbgap ) / 2, thick - delta, thick + pcb_z + pcb_size_mm[2]])
      rotate ([0, 0, 90])
         click (L = 10) ;

   translate ([thick + (pcb_size_mm[1] + 2*pcbgap ) / 2, thick + pcb_size_mm[0] + 2*pcbgap + delta, thick + pcb_z + pcb_size_mm[2]])
      rotate ([0, 0, -90])
         click (L = 10) ;
         
   disp_pt = [27.8, 9.6] ;
   disp_sz = [28, 12] ;
   disp_z = pcb_z - 1.2 ;
   
   translate ([disp_pt[0] - disp_sz[0]/2, disp_pt[1] - disp_sz[1]/2, thick - delta])
      cube ([disp_sz[0], disp_sz[1], disp_z]) ;
}

module case()
{
   difference()
   {
      case_base() ;
      
      translate ([thick + battery_offs[0] + battery_sz[1]/2 - 1, thick, thick - 1.5])
         cube ([2, battery_offs[1] + delta, case_sz[2]]) ;

      translate ([thick + battery_offs[0] + battery_sz[1]/2 - 1, case_sz[1] - thick - battery_offs[1] - delta, thick - 1.5])
         cube ([2, battery_offs[1] + delta, case_sz[2]]) ;

      translate ([thick + battery_offs[0] + battery_sz[1]/2 - 1, thick, thick - 1.5])
         cube ([2, case_sz[1] - 2*thick, 1.5 + delta]) ;

      translate ([thick + battery_offs[0] - 7, 48, thick - 1.5])
         cube ([battery_sz[1]/2 + 7, 4, 1.5 + delta]) ;
      
      translate ([thick + 27.75, -delta, thick + 3.9])
         cube ([9, thick + 2*delta, 3.5]) ;

      translate ([1, 22.6, thick + 0.5])
         cube ([thick, 6, case_sz[2]]) ;

      translate ([-delta, 22.6 + 1, thick + 1.5])
         cube ([thick + 2*delta, 4, 2.2]) ;
         
      hull()
      {
         disp_h = 1.2 ;
         disp_z = thick + pcb_z - disp_h ;
         
         translate ([15.8, 6.1, disp_z])
            cube ([24, 7, delta]) ;

         translate ([15.8 - disp_z, 6.1 - disp_z, -delta])
            cube ([24 + disp_z * 2, 7 + disp_z * 2, delta]) ;
      }
      
      translate ([thick + 5, 8 + pcb_size_mm[0] + thick, -delta])
         for (row = [0:4])
            for (col = [0:4])
               translate ([col * 6.5, row * 6.5, 0])
                  cylinder (d=1.5, h = thick + 2*delta) ;
            
      translate ([(case_sz[0] - cap_sz[0])/2 - stdgap, (case_sz[1] - cap_sz[1])/2 - stdgap, case_sz[2] - cap_sz[2]])
         rcube ([cap_sz[0] + 2*stdgap, cap_sz[1] + 2*stdgap, cap_sz[2] + delta], rdim = 0.5) ;

      {
         D = 9 ;
         translate ([(case_sz[0] - cap_sz[0])/2, (case_sz[1] - cap_sz[1])/2, case_sz[2] - cap_sz[2]])
            hull()
            {
               translate ([-15, case_sz[1] - 15, -D + 1 + D / 2])
                  rotate ([0, 90, 0])
                     cylinder (d = D, h = 20) ;
               
               translate ([-15, case_sz[1] - 15, 5])
                  rotate ([0, 90, 0])
                     cylinder (d = D, h = 20) ;
            }
      }
      
      translate ([-delta, 31, thick + pcb_z + pcb_size_mm[2] + 1.7])
         rotate ([0, 90, 0])
            cylinder (d = 1.5, h = 10) ;

      translate ([-delta, 36, thick + pcb_z + pcb_size_mm[2] + 1.7])
         rotate ([0, 90, 0])
            cylinder (d = 1.5, h = 10) ;
   }
}

module button()
{
   sz = [3.6, 1.8, 3] ;
   szb = sz + [2, 2, -2.5] ;

   difference()
   {
      union()
      {
         translate ([-sz[0]/2, -sz[1]/2, 0])
            rcube (sz, rdim = 0.3) ;

         translate ([-szb[0]/2, -szb[1]/2, 0])
            rcube (szb, rdim = 0.3) ;
      }
    
      translate ([-1.5, -szb[1]/2 - delta, -delta])
         cube ([3, szb[1]/2 + sz[1]/2 - 0.5, 0.5 + 2*delta]) ;
   }
}

module cap_base()
{
   D = 8 ;

   rcube (cap_sz, rdim = 0.5) ;

   translate ([-15, case_sz[1] - 15, -D + 1 + D / 2])
      rotate ([0, 90, 0])
         cylinder (d = D, h = 15 + 2 + delta) ;

   translate ([-(case_sz[0] - cap_sz[0])/2, case_sz[1] - 15 - 4, -D + 1 + D / 2])
      cube ([(case_sz[0] - cap_sz[0])/2 + 2 + delta, D, D/2 + thick - 1]) ;

   translate ([2, case_sz[1] - 15 - 4, -D + 1 + D / 2 - 4])
      cube ([sens_offs_x - 2, D, D]) ;

   translate ([sens_offs_x - D/2, case_sz[1] - 15 - 38, -D + 1])
      cube ([D, 38, D]) ;

   translate ([sens_offs_x - D/2, case_sz[1] - 15 - D/2, -D + 1])
      cube ([D, D, D]) ;
   
   translate ([sens_offs_x, 32.5, -13 + 1])
      cylinder (d = 22, h = 13) ;
}

module cap()
{
   D = 8 ;

   translate ([(case_sz[0] - cap_sz[0])/2, (case_sz[1] - cap_sz[1])/2, case_sz[2] - cap_sz[2]])
   {
      difference()
      {
         cap_base() ;

         translate ([sens_offs_x, 32.5, -13 + 1 - delta])
            cylinder (d = 20, h = 13 + delta) ;
         
         translate ([-15 - delta, case_sz[1] - 15, -D + 1 + D / 2])
            rotate ([0, 90, 0])
               cylinder (d = D - 3, h = sens_offs_x + 15) ;

         translate ([sens_offs_x, case_sz[1] - 15, -D + 1 + D / 2])
            rotate ([90, 0, 0])
               cylinder (d = D - 3, h = 38) ;
         
         translate ([sens_offs_x, 32.5, -delta])
            for (row = [0:2])
               for (a = [0:5])
               {
                  D = row * 4 ;
                  A = a * 60 + row * 30 ;
                  
                  translate ([D * cos (A), D * sin (A)])
                     cylinder (d = 1, h = thick + 2*delta) ;
               }

         translate ([sens_offs_x - (D - 3)/2, case_sz[1] - 15 - (D - 3)/2, -(D-3) -0.5])
            cube ([D - 3, D - 3, D - 3]) ;
      }
   }
}

module cap2()
{
translate ([(case_sz[0] - cap_sz[0])/2, (case_sz[1] - cap_sz[1])/2, case_sz[2] - cap_sz[2]])
   translate ([sens_offs_x, 32.5, -13])
   {
      difference()
      {
         cylinder (d = 20 - 2*stdgap, h = 7.5) ;
         
         translate ([0, 0, -1])
            cylinder (d = 20 - 2*stdgap - 2, h = 7.5) ;
         
         translate ([0, 0, 1])
            cylinder (d = 2, h = 7.5) ;
      }
      
      translate ([0, 0, 0.5])
         rotate_extrude()
            translate ([9.7, 0])
               circle (d = 1) ;
      
      translate ([-2, -2, 5])
         cube ([4, 4, 0.5]) ;

      translate ([-2, -2, 5])
         cube ([4, 0.5, 1.5 + delta]) ;

      translate ([-2, 1.5, 5])
         cube ([4, 0.5, 1.5 + delta]) ;
   }
}

module assembly()
{
   place_pcb()
      pcb() ;

   place_battery()
      bat18650() ;

   case() ;

   translate ([1.7, 25.6, thick + 2.6])
      rotate ([-90, 0, 90])
         button() ;

   translate ([10,10, 20])
   {
      cap() ;
     cap2() ;
   }
}

assembly() ;

// print case
//case() ;

// print button
//button() ;

// print cap
//rotate ([0, 180, 0])
//   cap() ;

// print cap2
//rotate ([0, 180, 0])
//   cap2() ;
