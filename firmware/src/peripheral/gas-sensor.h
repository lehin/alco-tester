////////////////////////////////////////////////////////////////////////////////////////////
//
// gas-sensor.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "hw/adc.h"
#include "aux/pgmstruct.h"

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

class GasSensor
{
public:
   void initialize (void) ;
   void shutdown (void) ;

   typedef void (*OnResistance) (float) ;
   void on_resistance (OnResistance fn) { m_on_resistance = fn ; }

   typedef void (*OnWorkrange) (uint8_t) ;
   void on_workrange (OnWorkrange fn) { m_on_workrange = fn ; }

private:
   void _measure_vint (void) ;
   void _schedule_measure (void) ;
   void _measure (void) ;
   void _on_value (uint16_t val) ;
   void _workrange (uint8_t r) ;

private:
   class Range
   {
   public:
      enum class Flags : uint8_t { Default = 0, Vint = 1, Kamp = 2 } ;

      friend constexpr bool operator& (Flags lhs, Flags rhs) { return ((uint8_t)lhs & (uint8_t)rhs) != 0 ; }
      friend constexpr Flags operator| (Flags lhs, Flags rhs) { return static_cast<Flags> (((uint8_t)lhs | (uint8_t)rhs)) ; }

      auto flags (void) const { return aux::pgms::proxy (m_flags) ; }
      auto lbound (void) const { return aux::pgms::proxy (m_lbound) ; }
      auto rbound (void) const { return aux::pgms::proxy (m_rbound) ; }

      constexpr Range (uint16_t lbound, uint16_t rbound, Flags flags = Flags::Default) :
         m_flags{ flags }, m_lbound{ lbound }, m_rbound{ rbound }
      {
      }

   private:
      Flags m_flags ;
      uint16_t m_lbound, m_rbound ;
   } ;

   uint8_t m_workrange = 0 ;
   OnWorkrange m_on_workrange = nullptr ;

   uint16_t m_vint ;

   OnResistance m_on_resistance = nullptr ;

   bool m_running = false ;
   bool m_measuring = false ;

   uint16_t m_main_schedule = 0,
            m_vint_schedule = 0 ;

   static const Range s_ranges[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern GasSensor GAS ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
