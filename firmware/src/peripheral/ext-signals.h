////////////////////////////////////////////////////////////////////////////////////////////
//
// ext-signals.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/interrupt.h>

ISR (PCINT1_vect) ;
ISR (PCINT2_vect) ;

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

class ExtSignals
{
public:
   ExtSignals (void) ;

   void enable (void) ;
   bool charging (void) const ;
   bool charger (void) const ;

   void lock_charging (void) ;
   void unlock_charging (void) ;

   void lock_button (void) ;
   void unlock_button (void) ;

   typedef void (*OnButton) (bool on, bool lpress) ;
   void on_button (OnButton fn) { m_on_button = fn ; }

   typedef void (*OnSignal) (bool on) ;
   void on_charger (OnSignal fn) { m_on_charger = fn ; }
   void on_charging (OnSignal fn) { m_on_charging = fn ; }

   typedef void (*OnPending) (bool pending) ;
   void on_pending (OnPending fn) { m_on_pending = fn ; }

private:
   void _check_signals (void) ;

   friend void ::PCINT1_vect (void) ;
   friend void ::PCINT2_vect (void) ;

   static void _on_pc_interrupt (void) ;

private:
   bool m_button : 1,
        m_longpress : 1,
        m_charger : 1,
        m_charging : 1 ;

   OnButton m_on_button = nullptr ;
   OnSignal m_on_charger = nullptr ;
   OnSignal m_on_charging = nullptr ;
   OnPending m_on_pending = nullptr ;

   uint16_t m_lpress_sched = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern ExtSignals EXTSIGNALS ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
