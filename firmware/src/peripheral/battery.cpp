////////////////////////////////////////////////////////////////////////////////////////////
//
// battery.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "battery.h"
#include "hw/adc.h"
#include "aux/scheduler.h"

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

static constexpr uint8_t INPUT = 0 ;

////////////////////////////////////////////////////////////////////////////////////////////

Battery BAT ;

////////////////////////////////////////////////////////////////////////////////////////////

void Battery::initialize (void)
{
   m_voltage = 420 ;
   m_lowbat = false ;
   _schedule_measure() ;
}

void Battery::shutdown (void)
{
   aux::Scheduler::cancel (m_schedule) ;
   m_schedule = 0 ;
}

void Battery::_schedule_measure (void)
{
   m_schedule =
      aux::Scheduler::schedule
         (
            []
            {
               if (!BAT.m_schedule)
                  return ;

               BAT._schedule_measure() ;
               BAT._measure() ;
            },
            500
         ) ;
}

void Battery::_measure (void)
{
   hw::Adc::measure (INPUT, hw::Adc::Vref::Avcc, [] (auto val){ BAT._on_measured (val) ; }) ;
}

void Battery::_on_measured (uint16_t val)
{
   m_voltage = 490ul * val / 1024 ;

   if (m_on_voltage)
      m_on_voltage (m_voltage) ;

   static constexpr uint16_t V_LOWBAT = 320 ;

   if (m_lowbat || m_voltage > V_LOWBAT)
      return ;

   m_lowbat = true ;

   if (m_on_lowbat)
      m_on_lowbat() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
