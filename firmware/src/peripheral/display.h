////////////////////////////////////////////////////////////////////////////////////////////
//
// display.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/pgmstruct.h"

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

class Display
{
public:
   static constexpr uint8_t WIDTH = 128 ;
   static constexpr uint8_t HEIGHT = 4 ;
   static constexpr uint8_t SYM_WIDTH = 16 ;
   static constexpr uint8_t SYM_HEIGHT = 3 ;

public:
   static void initialize (void) ;
   static void clear (void) ;

   static void putc (uint8_t x, uint8_t y, char ch) ;
   static void digit (uint8_t x, uint8_t y, uint8_t num) ;
   static void battery (uint8_t x, uint8_t y, uint8_t num) ;
   static void range (uint8_t x, uint8_t y, uint8_t num) ;
   static void ready (uint8_t x, uint8_t y, bool ready) ;

private:
   static void _raw_symbol (uint8_t font, uint8_t x, uint8_t y, uint8_t sym) ;

private:
   class Font
   {
   public:
      auto width (void) const { return aux::pgms::proxy (m_width) ; }
      auto height (void) const { return aux::pgms::proxy (m_height) ; }
      auto data (void) const { return aux::pgms::proxy (m_data) ; }

      constexpr Font (uint8_t w, uint8_t h, const uint8_t* d) : m_width{ w }, m_height{ h }, m_data{ d } {}

   private:
      uint8_t m_width, m_height ;
      const uint8_t* m_data ;
   } ;

   static const Font m_fonts[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
