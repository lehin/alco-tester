////////////////////////////////////////////////////////////////////////////////////////////
//
// gas-sensor.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "gas-sensor.h"
#include "aux/scheduler.h"
#include "hw/gpio.h"

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

static constexpr uint8_t INPUT = 7 ;
static constexpr float R0 = 100 ;

////////////////////////////////////////////////////////////////////////////////////////////

GasSensor GAS ;

const GasSensor::Range GasSensor::s_ranges[] PROGMEM =
{
   { 180, 1024 },
   { 500, 1000, Range::Flags::Vint },
   { 180, 800, Range::Flags::Kamp },
   { 0, 1000, Range::Flags::Vint | Range::Flags::Kamp }
} ;

void GasSensor::initialize (void)
{
   m_running = true ;
   m_measuring = false ;
   _measure_vint() ;
   _schedule_measure() ;
}

void GasSensor::shutdown (void)
{
   m_running = false ;
   m_measuring = false ;
   aux::Scheduler::cancel (m_main_schedule) ;
   aux::Scheduler::cancel (m_vint_schedule) ;
   m_main_schedule = m_vint_schedule = 0 ;
}

void GasSensor::_measure_vint (void)
{
   if (!m_running)
      return ;

   m_vint_schedule = aux::Scheduler::schedule ([] { GAS._measure_vint() ; }, 1000) ;
   hw::Adc::measure (14, hw::Adc::Vref::Avcc, [] (auto val) { if (GAS.m_running) GAS.m_vint = val ; }) ;
}

void GasSensor::_schedule_measure (void)
{
   if (!m_running)
      return ;

   m_main_schedule =
      aux::Scheduler::schedule
         (
            []
            {
               GAS._schedule_measure() ;

               if (!GAS.m_measuring)
               {
                  GAS.m_measuring = true ;
                  GAS._measure() ;
               }
            },
            50
         ) ;
}

void GasSensor::_measure (void)
{
   if (!m_running)
      return ;

   hw::Adc::measure
      (
         INPUT,
         s_ranges[m_workrange].flags() & Range::Flags::Vint ? hw::Adc::Vref::Int : hw::Adc::Vref::Avcc,
         [] (auto val)
         {
            GAS._on_value (val) ;
         }
      ) ;
}

void GasSensor::_on_value (uint16_t val)
{
   if (!m_running)
      return ;

   const auto& r = s_ranges[m_workrange] ;

   if (val < r.lbound())
      _workrange (m_workrange + 1) ;
   else if (val > r.rbound())
      _workrange (m_workrange - 1) ;
   else
   {
      m_measuring = false ;

      static constexpr float REFN = 1074 ;

      auto v = val / (r.flags() & Range::Flags::Kamp ? 12.0f : 2.0f) ;
      auto k = r.flags() & Range::Flags::Vint ? REFN / m_vint : 1 ;
      auto r = v * R0 / (REFN * k - v) ;

      if (m_on_resistance)
         m_on_resistance (r) ;
   }
}

void GasSensor::_workrange (uint8_t r)
{
   m_workrange = r ;

   if (s_ranges[m_workrange].flags() & Range::Flags::Kamp)
      hw::gpio::Kamp::set() ;
   else
      hw::gpio::Kamp::reset() ;

   if (m_on_workrange)
      m_on_workrange (m_workrange) ;

   _measure() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
