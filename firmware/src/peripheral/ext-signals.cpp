////////////////////////////////////////////////////////////////////////////////////////////
//
// ext-signals.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "ext-signals.h"
#include "beeper.h"
#include "aux/scheduler.h"
#include "aux/ll-scheduler.h"
#include "hw/gpio.h"
#include "hw/power.h"

ISR (PCINT1_vect)
{
   peripheral::ExtSignals::_on_pc_interrupt() ;
}

ISR (PCINT2_vect)
{
   peripheral::ExtSignals::_on_pc_interrupt() ;
}

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

ExtSignals EXTSIGNALS ;

////////////////////////////////////////////////////////////////////////////////////////////

ExtSignals::ExtSignals (void) :
   m_button{ false },
   m_longpress{ false },
   m_charger{ false },
   m_charging{ false }
{
}

void ExtSignals::enable (void)
{
   PCMSK1 = _BV(PCINT9) ;
   PCMSK2 = _BV(PCINT18) | _BV(PCINT19) ;
   PCICR = _BV(PCIE1) | _BV(PCIE2) ;
   _check_signals() ;
}

void ExtSignals::lock_charging (void)
{
   PCMSK1 &= ~_BV(PCINT9) ;
   hw::gpio::Chrg::reset() ;
   m_charging = false ;
}

void ExtSignals::unlock_charging (void)
{
   hw::gpio::Chrg::set() ;
   PCMSK1 |= _BV(PCINT9) ;
   aux::LowLatencySched::schedule ([] { EXTSIGNALS._check_signals() ; }) ;
}

void ExtSignals::lock_button (void)
{
   PCMSK2 &= ~_BV(PCINT18) ;
}

void ExtSignals::unlock_button (void)
{
   PCMSK2 |= _BV(PCINT18) ;
}

bool ExtSignals::charging (void) const
{
   return !hw::gpio::Chrg::read() ;
}

bool ExtSignals::charger (void) const
{
   return hw::gpio::Vchrg::read() ;
}

void ExtSignals::_check_signals (void)
{
   bool s =  !hw::gpio::Btn::read() ;
   if ((PCMSK2 & _BV(PCINT18)) && m_button != s)
   {
      m_button = s ;

      if (m_lpress_sched)
      {
         aux::Scheduler::cancel (m_lpress_sched) ;
         m_lpress_sched = 0 ;
      }

      if (m_button)
      {
         Beeper::beep (Beeper::Type::Single) ;

         m_lpress_sched =
            aux::Scheduler::schedule
               (
                  []
                  {
                     if (!EXTSIGNALS.m_lpress_sched)
                        return ;

                     EXTSIGNALS.m_lpress_sched = 0 ;
                     EXTSIGNALS.m_longpress = true ;

                     Beeper::beep (Beeper::Type::Single) ;

                     if (EXTSIGNALS.m_on_button)
                        EXTSIGNALS.m_on_button (EXTSIGNALS.m_button, EXTSIGNALS.m_longpress) ;
                  },
                  1000
               ) ;
      }

      if (m_on_button)
         m_on_button (m_button, m_longpress) ;

      m_longpress = false ;
   }

   s = charger() ;
   if (m_charger != s)
   {
      m_charger = s ;
      if (m_on_charger)
         m_on_charger (m_charger) ;
   }

   s = charging() ;
   if ((PCMSK1 & _BV(PCINT9)) && m_charging != s)
   {
      m_charging = s ;
      if (m_on_charging)
         m_on_charging (m_charging) ;
   }
}

void ExtSignals::_on_pc_interrupt (void)
{
   static bool _handling = false ;

   if (_handling)
      return ;

   _handling = true ;

   aux::LowLatencySched::schedule
      (
         []
         {
            hw::POWER.run() ;

            if (EXTSIGNALS.m_on_pending)
               EXTSIGNALS.m_on_pending (true) ;
         }
      ) ;

   aux::Scheduler::schedule
      (
         []
         {
            _handling = false ;

            EXTSIGNALS._check_signals() ;

            if (EXTSIGNALS.m_on_pending)
               EXTSIGNALS.m_on_pending (false) ;
         },
         50
      ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
