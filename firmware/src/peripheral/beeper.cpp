////////////////////////////////////////////////////////////////////////////////////////////
//
// beeper.cpp
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "beeper.h"
#include "aux/scheduler.h"
#include <avr/io.h>

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

const Beeper::Control Beeper::s_beeps[(uint8_t)Beeper::Type::_Count] PROGMEM
{
   { 8000, 0, 0 },
   { 20, 100, 1 },
   { 20, 100, 2 },
   { 20, 100, 3 }
} ;

Beeper::Control Beeper::m_control ;
uint16_t Beeper::m_schedule ;

SINGLETON_IMPL (Beeper) ;

Beeper::Beeper (void)
{
   OCR0A = 250 ;
   OCR0B = 250 / 2 ;
   TCCR0A = _BV(WGM00) | _BV(WGM01) ;
   TCCR0B = _BV(WGM02) | _BV(CS01) ;
}

void Beeper::beep (Type type)
{
   auto ctrl = s_beeps + (uint8_t)type ;
   m_control.m_pulse = pgm_read_word (&ctrl->m_pulse) ;
   m_control.m_period = pgm_read_word (&ctrl->m_period) ;
   m_control.m_count = pgm_read_word (&ctrl->m_count) ;

   if (m_schedule)
      aux::Scheduler::cancel (m_schedule) ;

   _on() ;
}

void Beeper::mute (void)
{
   if (m_schedule)
      aux::Scheduler::cancel (m_schedule) ;

   m_control.m_pulse = 0 ;
   _off() ;
}

void Beeper::_on (void)
{
   if (!m_control.m_pulse)
   {
      _off() ;
      return ;
   }

   TCCR0A |= _BV(COM0B1) ;
   m_schedule = aux::Scheduler::schedule (_off, m_control.m_pulse) ;
}

void Beeper::_off (void)
{
   m_schedule = 0 ;

   if (!m_control.m_pulse || (m_control.m_count && !--m_control.m_count))
   {
      TCCR0A &= ~_BV(COM0B1) ;
      m_control.m_pulse = 0 ;
      return ;
   }

   auto pause = m_control.m_period - m_control.m_pulse ;
   if (!pause)
      _on() ;
   else
   {
      TCCR0A &= ~_BV(COM0B1) ;
      m_schedule = aux::Scheduler::schedule (_on, pause) ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
