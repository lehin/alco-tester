////////////////////////////////////////////////////////////////////////////////////////////
//
// display.cpp
//
// Copyright © 2018 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "display.h"
#include "hw/i2c.h"
#include "hw/gpio.h"
#include "aux/scheduler.h"
#include <avr/pgmspace.h>

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

   namespace
   {
      static constexpr uint8_t ADDRESS = 0x3c ;

      static void _cmd_byte (hw::I2c::Tx& tx, uint8_t byte)
      {
         tx << (uint8_t)0x80 << byte ;
      }

      static void _window (uint8_t x1, uint8_t x2, uint8_t y1, uint8_t y2)
      {
         auto tx = hw::I2c::tx() ;
         _cmd_byte (tx, 0x21) ; _cmd_byte (tx, x1) ; _cmd_byte (tx, x2 - 1) ;
         _cmd_byte (tx, 0x22) ; _cmd_byte (tx, y1) ; _cmd_byte (tx, y2 - 1) ;
         hw::I2c::send (ADDRESS) ;
      }

      #include "fonts/font.inc"
      #include "fonts/digits.inc"
   }

////////////////////////////////////////////////////////////////////////////////////////////

const Display::Font Display::m_fonts[] PROGMEM
{
   { s_small_width, s_small_height, s_small_font },
   { s_digits_width, s_digits_height, s_digits }
} ;

void Display::initialize (void)
{
   static const uint8_t s_led_init[] PROGMEM
   {
      0xAE,
      0x20, 0x01,
      0xB0,
      0xC0,
      0x00,
      0x10,
      0x40,
      0x81, 0xff,
      0xA0,
      0xA6,
      0xA8, 0x1F,
      0xA4,
      0xD3, 0x00,
      0xD5, 0xF0,
      0xD9, 0x22,
      0xDA, 0x02,
      0xDB, 0x20,
      0x8D, 0x14,
      0x2E,
      0xAF
   } ;

   hw::gpio::Drst::set() ;

   aux::Scheduler::schedule
      (
         []
         {
            hw::gpio::Drst::reset() ;

            auto tx = hw::I2c::tx() ;

            for (auto& b : s_led_init)
               _cmd_byte (tx, pgm_read_byte (&b)) ;

            hw::I2c::send (ADDRESS) ;

            clear() ;
         },
         2
      ) ;
}

void Display::clear (void)
{
   _window (0, WIDTH, 0, HEIGHT) ;

   for (uint8_t n = 0; n < (WIDTH / 32) * HEIGHT; ++n)
   {
      auto tx = hw::I2c::tx() ;
      tx << (uint8_t)0x40 ;

      for (uint8_t bulk = 0; bulk < 32; ++bulk)
         tx << (uint8_t)0x00 ;

      hw::I2c::send (ADDRESS) ;
   }
}

void Display::putc (uint8_t x, uint8_t y, char ch)
{
   _raw_symbol (0, x,y, ch - 0x20) ;
}

void Display::digit (uint8_t x, uint8_t y, uint8_t num)
{
   _raw_symbol (1, x,y, num) ;
}

void Display::battery (uint8_t x, uint8_t y, uint8_t num)
{
   static constexpr uint8_t WIDTH = 16 ;
   static constexpr uint8_t HEIGHT = 4 ;

   _window (x, x + WIDTH, y, y + HEIGHT) ;

   auto tx = hw::I2c::tx() ;
   tx << (uint8_t)0x40 ;

   for (uint8_t col = 0; col < WIDTH; ++col)
      for (uint8_t row = 0; row < HEIGHT; ++row)
      {
         uint8_t lvl = 0 ;

         if (col < 2 || col >= 14)
            lvl = row ? 8 : 6;
         else
         {
            uint8_t rlvl = (HEIGHT - row - 1) * 8 ;

            if (num > rlvl)
               lvl = num - rlvl ;

            if (lvl > 8)
               lvl = 8 ;
         }

         uint8_t byte = 0 ;

         if (lvl)
            byte = ~((0b10000000 >> (lvl - 1)) - 1) ;

         if (!row)
         {
            byte |= 0b00001100 ;
            if (col >= 7 && col <= 10)
               byte |= 0b00000011 ;
         }
         else if (row == HEIGHT - 1)
            byte |= 0b11000000 ;

         tx << byte ;
      }

   hw::I2c::send (ADDRESS) ;
}

void Display::range (uint8_t x, uint8_t y, uint8_t num)
{
   static constexpr uint8_t DOTBYTES = 9 ;

   static const uint8_t _small[DOTBYTES] PROGMEM =
   {
      0b00000000, 0b00000000, 0b00000000, 0b00010000, 0b00111000,
      0b00010000, 0b00000000, 0b00000000, 0b00000000
   } ;

   static const uint8_t _large[DOTBYTES] PROGMEM =
   {
      0b00000000, 0b00010000, 0b00010000, 0b00111000, 0b11111110,
      0b00111000, 0b00010000, 0b00010000, 0b00000000
   } ;

   static const uint8_t WIDTH = DOTBYTES * 4 ;
   static const uint8_t HEIGHT = 1 ;

   _window (x, x + WIDTH, y, y + HEIGHT) ;

   auto tx = hw::I2c::tx() ;
   tx << (uint8_t)0x40 ;

   for (uint8_t dot = 0; dot < 4; ++dot)
   {
      auto data = dot == num ? _large : _small ;
      for (uint8_t n = 0; n < DOTBYTES; ++n)
         tx << pgm_read_byte (data++) ;
   }

   hw::I2c::send (ADDRESS) ;
}

void Display::ready (uint8_t x, uint8_t y, bool ready)
{
   static const uint8_t _busy[] PROGMEM =
   {
      0b00000000, 0b00000000, 0b00000000, 0b11000011,
      0b10100101, 0b11111101, 0b10100101, 0b11000011
   } ;

   static const uint8_t _ready[] PROGMEM =
   {
      0b00011100, 0b01110000, 0b11000000, 0b01110000,
      0b00011000, 0b00001100, 0b00000110, 0b00000011
   } ;

   static const uint8_t WIDTH = sizeof (_busy) ;

   _window (x, x + WIDTH, y, y + 1) ;

   auto tx = hw::I2c::tx() ;
   tx << (uint8_t)0x40 ;

   auto p = ready ? _ready : _busy ;

   for (uint8_t n = 0; n < WIDTH; ++n)
      tx << pgm_read_byte (p++) ;

   hw::I2c::send (ADDRESS) ;
}

void Display::_raw_symbol (uint8_t font, uint8_t x, uint8_t y, uint8_t sym)
{
   auto width = m_fonts[font].width(),
        height = m_fonts[font].height() ;

   const uint8_t *data = m_fonts[font].data() + (uint16_t)sym * width * height ;

   _window (x, x + width, y, y + height) ;

   auto tx = hw::I2c::tx() ;
   tx << (uint8_t)0x40 ;

   for (uint8_t n = 0; n < height * width; ++n)
      tx << pgm_read_byte (data++) ; ;

   hw::I2c::send (ADDRESS) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
