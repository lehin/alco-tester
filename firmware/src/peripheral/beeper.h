////////////////////////////////////////////////////////////////////////////////////////////
//
// beeper.h
//
// Copyright © 2017 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include <stdint.h>
#include <avr/pgmspace.h>

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

class Beeper
{
   SINGLETON_DECL (Beeper) ;

public:
   enum class Type { On, Single, Double, Triple, _Count } ;

public:
   Beeper (void) ;

   static void beep (Type type) ;
   static void mute (void) ;

private:
   static void _on (void) ;
   static void _off (void) ;

private:
   struct Control
   {
      uint16_t m_pulse,
               m_period ;
      uint8_t m_count ;
   } ;

   static Control m_control ;
   static uint16_t m_schedule ;

   static const Control s_beeps[] PROGMEM ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
