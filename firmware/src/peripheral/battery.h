////////////////////////////////////////////////////////////////////////////////////////////
//
// battery.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

namespace peripheral
{

////////////////////////////////////////////////////////////////////////////////////////////

class Battery
{
public:
   void initialize (void) ;
   void shutdown (void) ;

   uint16_t voltage (void) const { return m_voltage ; }

   typedef void (*OnVoltage) (uint16_t) ;
   void on_voltage (OnVoltage fn) { m_on_voltage = fn ; }

   typedef void (*OnLowBat) (void) ;
   void on_lowbat (OnLowBat fn) { m_on_lowbat = fn ; }

private:
   void _schedule_measure (void) ;
   void _measure (void) ;
   void _on_measured (uint16_t val) ;

private:
   uint16_t m_voltage ;
   uint16_t m_schedule ;

   OnVoltage m_on_voltage = nullptr ;

   bool m_lowbat ;
   OnLowBat m_on_lowbat = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern Battery BAT ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace peripheral
