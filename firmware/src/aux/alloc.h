////////////////////////////////////////////////////////////////////////////////////////////
//
// alloc.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "minmax.h"
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

uint8_t *allocate_heap_memory (size_t size) ;

////////////////////////////////////////////////////////////////////////////////////////////

template<size_t _Sz, size_t _Chunk = 1>
   class PoolAlloc
   {
      static_assert (_Sz < 256, "Too large struct to use Allocator class") ;

      struct Free
      {
         Free* m_next ;
      } ;

      static constexpr uint8_t item_size = max (_Sz, sizeof (Free)) ;
      static constexpr uint8_t chunk_size = _Chunk ;

   public:
      PoolAlloc (void)
      {
      }

      static void* allocate (void) ;
      static void free (void *mem) ;

   private:
      static bool _allocate_chunk (void) ;

   private:
      static Free* s_head ;
   } ;


template<size_t _Sz, size_t _Chunk>
   typename PoolAlloc<_Sz,_Chunk>::Free *PoolAlloc<_Sz,_Chunk>::s_head = nullptr ;

template<size_t _Sz, size_t _Chunk> inline
   void *PoolAlloc<_Sz,_Chunk>::allocate (void)
   {
      do
      {
         if (s_head)
         {
            void *block = s_head ;
            s_head = s_head->m_next ;
            return block ;
         }
      } while (_allocate_chunk()) ;

      return nullptr ;
   }

template<size_t _Sz, size_t _Chunk> inline
   void PoolAlloc<_Sz,_Chunk>::free (void *mem)
   {
      auto chunk = reinterpret_cast<Free*> (mem) ;
      chunk->m_next = s_head ;
      s_head = chunk ;
   }


template<size_t _Sz, size_t _Chunk>
   bool PoolAlloc<_Sz,_Chunk>::_allocate_chunk (void)
   {
      auto chunk = allocate_heap_memory (chunk_size * item_size) ;
      if (!chunk)
         return false ;

      auto last = chunk + (chunk_size - 1) * item_size ;

      for (auto it = chunk; it != last; it += item_size)
         reinterpret_cast<Free*> (it) -> m_next = reinterpret_cast<Free*> (it + item_size) ;

      reinterpret_cast<Free*> (last) -> m_next = s_head ;
      s_head = reinterpret_cast<Free*> (chunk) ;

      return true ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp, size_t _Chunk = 1>
   class PoolAllocated
   {
   public:
      static void *operator new (size_t sz)
         { return PoolAlloc<sizeof(_Tp),_Chunk>::allocate() ; }

      static void operator delete (void *p)
         { return PoolAlloc<sizeof(_Tp),_Chunk>::free (p) ; }
   } ;

///////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
