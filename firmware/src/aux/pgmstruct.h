////////////////////////////////////////////////////////////////////////////////////////////
//
// pgmstruct.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/pgmspace.h>

namespace aux
{
   namespace pgms
   {

////////////////////////////////////////////////////////////////////////////////////////////

template<size_t _Sz>
   struct Reader
   {
      struct retval
      {
         uint8_t arr[_Sz] ;
      } ;

      static retval read (const void *addr)
      {
         auto a = (const uint8_t*)addr ;

         retval ret ;

         for (auto& b : ret.arr)
            b = pgm_read_byte (a++) ;

         return ret ;
      }
   } ;

template<>
   struct Reader<1>
   {
      static uint8_t read (const void *addr) { return pgm_read_byte (addr) ; }
   } ;

template<>
   struct Reader<2>
   {
      static uint16_t read (const void *addr) { return pgm_read_word (addr) ; }
   } ;

template<>
   struct Reader<4>
   {
      static uint32_t read (const void *addr) { return pgm_read_dword (addr) ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   struct Proxy
   {
      typedef _Tp value ;
      typedef const value& reference ;

      reference m_val ;
      Proxy (reference& val) : m_val{ val } {}

      value get (void) const
      {
         typedef Reader<sizeof(value)> R ;

         union
         {
            decltype (R::read (&m_val)) m_raw ;
            _Tp m_typed ;
         } cvt{ R::read (&m_val) } ;

         return cvt.m_typed ;
      }
      operator value (void) const { return get() ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   inline Proxy<_Tp> proxy (const _Tp& p)  { return p ; }

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace pgms
} // namespace aux
