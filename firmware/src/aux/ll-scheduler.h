////////////////////////////////////////////////////////////////////////////////////////////
//
// ll-scheduler.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "ring-buffer.h"
#include <util/atomic.h>
#include <avr/io.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

class LowLatencySched
{
   typedef void (*Slot) (void) ;

public:
   static void schedule (const Slot& slot) ;
   static void process (void) ;
   static void process_no_sleep (void) ;

   template<typename _Cond>
      static void wait (_Cond cond)
      {
         while (!cond())
            process_no_sleep() ;
      }

private:
   typedef RingBuffer<32,Slot> Queue ;
   static Queue s_pending ;
} ;


inline
   void LowLatencySched::schedule (const Slot& slot)
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         s_pending.push_back (slot) ;
      }
   }

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Cond>
   inline void wait (_Cond cond)
      { LowLatencySched::wait (cond) ; }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
