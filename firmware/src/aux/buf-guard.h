////////////////////////////////////////////////////////////////////////////////////////////
//
// buf-guard.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <avr/interrupt.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Buffer>
   class BufferGuard
   {
      typedef _Buffer buffer_type ;
      BufferGuard (const BufferGuard&) = delete ;
      void operator= (const BufferGuard&) = delete ;
      
   public:
      typedef typename buffer_type::value_type value_type ;
      
   public:
      BufferGuard (buffer_type& buffer) : 
         m_buffer { &buffer },
         m_saved_state{ SREG }
      {
         cli() ;
         __asm__ volatile ("" ::: "memory") ;
      }

      BufferGuard (BufferGuard&& org) :
         m_buffer { org.m_buffer },
         m_saved_state { org.m_saved_state }
      {
         org.m_buffer = nullptr ;
      }
      
      ~BufferGuard (void)
      {
         if (m_buffer)
         {
            SREG = m_saved_state ;
            __asm__ volatile ("" ::: "memory") ;
         }
      }
      
      buffer_type* operator-> (void) { return m_buffer ; }
      
   private:
      buffer_type *m_buffer ;
      uint8_t m_saved_state ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
