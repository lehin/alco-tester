////////////////////////////////////////////////////////////////////////////////////////////
//
// range.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "type-traits.h"
#include <stddef.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _It>
   class Range
   {
   public:
      typedef _It iterator ;
      typedef typename iterator_value<iterator>::type value_type ;

   public:
      Range (const iterator& first, const iterator& last) :
         m_first (first), m_last (last) {}

      template<typename _It2>
         Range (const Range<_It2>& org) :
            m_first (org.begin()), m_last (org.end()) {}

      Range (void) : m_first{ nullptr }, m_last{ nullptr } {}

      iterator begin (void) const { return m_first ; }
      iterator end (void) const { return m_last ; }

      iterator find (const value_type& val) const ;

      template<typename _Fn>
         iterator find_if (const _Fn& fn) const ;

      bool empty (void) const { return m_last == m_first ; }
      size_t size (void) const { return m_last - m_first ; }

      value_type& operator[] (size_t idx) const { return begin()[idx] ; }

      template<typename _It2>
         static Range<_It> cast_from (const Range<_It2>& org)
            { return { reinterpret_cast<_It> (org.begin()), reinterpret_cast<_It> (org.end()) } ; }

   private:
      iterator m_first ;
      iterator m_last ;
   } ;


template<typename _It> inline
   Range<_It> range (const _It& first, const _It& last)
      { return Range<_It> { first, last } ; }

template<typename _Tp>
   struct RangeByBuf ;

template<typename _Tp, size_t _Sz>
   struct RangeByBuf<_Tp[_Sz]>
   {
      typedef _Tp *iterator ;
      static constexpr size_t size = _Sz ;
   } ;

template<typename _Tp> inline
   auto range (_Tp& buf) -> Range< typename RangeByBuf<_Tp>::iterator >
   {
      return Range< typename RangeByBuf<_Tp>::iterator > (buf + 0, buf + (RangeByBuf<_Tp>::size)) ;
   }

template<typename _It>
   typename Range<_It>::iterator Range<_It>::find (const value_type& val) const
   {
      iterator res = begin() ;
      for (; res != end(); ++res)
         if (*res == val)
            break ;

      return res ;
   }

template<typename _It>
   template<typename _Fn>
      typename Range<_It>::iterator Range<_It>::find_if (const _Fn& fn) const
      {
         iterator res = begin() ;
         for (; res != end(); ++res)
            if (fn (*res))
               break ;

         return res ;
      }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
