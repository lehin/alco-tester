////////////////////////////////////////////////////////////////////////////////////////////
//
// string.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "debug-assert.h"
#include "minmax.h"
#include "alloc.h"
#include "range.h"
#include <stdlib.h>
#include <string.h>
#include <util/atomic.h>

////////////////////////////////////////////////////////////////////////////////////////////

inline void* operator new (size_t, void* ptr) { return ptr ; }

////////////////////////////////////////////////////////////////////////////////////////////

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

typedef Range<const char*> StringRef ;

////////////////////////////////////////////////////////////////////////////////////////////

class String
{
public:
   typedef uint8_t size_type ;

   typedef char value_type ;
   typedef char* iterator ;
   typedef const char* const_iterator ;

private:
   struct Impl
   {
      size_type m_capacity ;
      size_type m_length ;
      uint8_t m_refcnt ;
      char m_data[] ;

      Impl (size_type capacity, size_type len) : m_capacity{ capacity }, m_length{ len }, m_refcnt{ 1 } {}
   } ;

   Impl *m_impl ;

public:
   String (void) :
      m_impl{ nullptr }
   {
   }

   String (size_type capacity, size_type len = 0) :
      m_impl { new (aux::allocate_heap_memory (sizeof (Impl) + capacity + 1)) Impl{ capacity, len } }
   {
   }

   String (const StringRef& str) :
      m_impl { new (aux::allocate_heap_memory (sizeof (Impl) + str.size() + 1)) Impl{ (size_type)str.size(), (size_type)str.size() } }
   {
      memcpy (begin(), str.begin(), str.size()) ;
   }

   String (const String& rhs) :
      m_impl{ rhs.m_impl }
   {
      _increment() ;
   }

   ~String (void)
   {
      _decrement() ;
   }

   String& operator= (const String& rhs)
   {
      _decrement() ;

      m_impl = rhs.m_impl ;
      _increment() ;

      return *this ;
   }

   static String from_progmem (const char *pstr, size_type extra_size = 0) ;

   iterator begin (void) { return m_impl ? m_impl->m_data : nullptr ; }
   iterator end (void) { return m_impl ? m_impl->m_data + m_impl->m_length : nullptr ; }

   const_iterator begin (void) const { return m_impl ? m_impl->m_data : nullptr ; }
   const_iterator end (void) const { return m_impl ? m_impl->m_data + m_impl->m_length : nullptr ; }

   bool empty (void) const { return !m_impl || m_impl->m_length == 0 ; }
   bool full (void) const { return !m_impl || m_impl->m_length == m_impl->m_capacity ; }
   size_type size (void) const { return m_impl ? m_impl->m_length : 0 ; }
   size_type length (void) const { return size() ; }
   void clear (void) { if (m_impl) m_impl->m_length = 0 ; }

   void resize (size_type newsize)
   {
      if (!m_impl)
         return ;

      if (newsize <= capacity())
         m_impl->m_length = newsize ;
   }

   bool push_back (value_type val)
   {
      if (full())
         return false ;

      m_impl->m_data[m_impl->m_length++] = val ;

      return true ;
   }

   void pop_back (void)
   {
      if (empty())
         return ;

      --m_impl->m_length ;
   }

   value_type& front (void) { return *begin() ; }
   const value_type& front (void) const { return *begin() ; }

   value_type& back (void) { return *(end() - 1) ; }
   const value_type& back (void) const { return *(end() - 1) ; }

   value_type& at (size_type idx)
      { return m_impl->m_data[idx] ; }

   const value_type& at (size_type idx) const
      { return m_impl->m_data[idx] ; }

   value_type& operator[] (size_type idx) { return at (idx) ; }
   const value_type& operator[] (size_type idx) const { return at (idx) ; }

   size_type capacity (void) const { return m_impl ? m_impl->m_capacity : 0 ; }

   void reset (void) { _decrement() ; }

   const char *c_str (void) const
   {
      if (!m_impl)
         return nullptr ;

      m_impl->m_data[ m_impl->m_length ] = 0 ;
      return m_impl->m_data ;
   }

   bool operator== (const String& rhs) const
   {
      return length() == rhs.length() && (!length() || !memcmp (begin(), rhs.begin(), length())) ;
   }

   bool operator== (const StringRef& ref) const
   {
      return length() == ref.size() && (!length() || !memcmp (begin(), ref.begin(), length())) ;
   }

   inline friend bool operator== (const StringRef& lhs, const aux::String& rhs)
      { return rhs == lhs ; }

private:
   void _increment (void)
   {
      if (m_impl)
         ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
         {
            ++m_impl->m_refcnt ;
         }
   }

   void _decrement (void)
   {
      if (!m_impl)
         return ;

      bool del ;

      ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
      {
         del = !--m_impl->m_refcnt ;
      }

      if (del)
         free (m_impl) ;

      m_impl = nullptr ;
   }
} ;

////////////////////////////////////////////////////////////////////////////////////////////

inline
   StringRef range (const String& str)
   {
      return { str.begin(), str.end() } ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
