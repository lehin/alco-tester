////////////////////////////////////////////////////////////////////////////////////////////
//
// function.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "forward.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Signature>
   class function ;

template<typename _Result, typename... _Args>
   class function<_Result(_Args...)>
   {
      struct Caller
      {
         virtual _Result call (_Args... args) = 0 ;
         virtual void destroy (void) = 0 ;
         virtual Caller* clone (void) const = 0 ;
      } ;

      template<typename _Fn>
         struct CallerImpl : Caller
         {
            _Fn m_fn ;
            CallerImpl (_Fn fn) : m_fn { fn } {}

            virtual _Result call (_Args... args) { return m_fn (args...) ; }
            virtual void destroy (void) { delete this ; }
            virtual Caller* clone (void) const { return new CallerImpl<_Fn> { m_fn } ; }
         } ;

         function (const function&) = delete ;
         int operator= (const function&) = delete ;

   public:
      template<typename _Fn>
         function (_Fn fn) :
            m_caller { new CallerImpl<_Fn> { fn } }
         {
         }

      function (void) :
         m_caller { nullptr }
      {
      }

      function (function&& org) :
         m_caller { org.m_caller }
      {
         org.m_caller = nullptr ;
      }

      ~function (void)
      {
         _destroy() ;
      }

      function& operator= (function&& rhs)
      {
         _destroy() ;

         m_caller = rhs.m_caller ;
         rhs.m_caller = nullptr ;

         return *this ;
      }

      _Result operator() (_Args... args) const
      {
         if (!m_caller)
            return _Result() ;

         return m_caller->call (args...) ;
      }

      bool is_null (void) const
      {
         return m_caller == nullptr ;
      }

      operator bool (void) const
      {
         return !is_null() ;
      }

   private:
      void _destroy (void)
      {
         if (m_caller)
            m_caller->destroy() ;
      }

   private:
      Caller *m_caller ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
