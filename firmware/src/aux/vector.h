////////////////////////////////////////////////////////////////////////////////////////////
//
// vector.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "alloc.h"
#include "debug-assert.h"

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   class Vector
   {
      Vector (const Vector&) = delete ;
      Vector& operator= (const Vector&) = delete ;

   public:
      typedef _Tp value_type ;
      typedef _Tp* iterator ;
      typedef const _Tp* const_iterator ;

   public:
      Vector (void) : m_data{ nullptr }, m_size{ 0 } {}
      Vector (uint8_t size) : m_size{ size } { _init() ; }
      ~Vector (void) { _destroy() ; }

      void init (uint8_t size)
      {
         _destroy() ;

         m_size = size ;
         _init() ;
      }

      uint8_t size (void) const { return m_size ; }

      iterator begin (void) { return m_data ; }
      iterator end (void) { return m_data + m_size ; }

      const_iterator begin (void) const { return m_data ; }
      const_iterator end (void) const { return m_data + m_size ; }

      value_type& operator[] (uint8_t n) { return at (n) ; }
      const value_type& operator[] (uint8_t n) const { return at (n) ; }

      value_type& at (uint8_t n) { aux::debug_assert (n < size()) ; return m_data[n] ; }
      const value_type& at (uint8_t n) const { const_cast<Vector*>(this)->at(n) ; }

   private:
      void _init (void)
      {
         m_data = reinterpret_cast<_Tp*> (allocate_heap_memory (sizeof (value_type) * m_size)) ;

         for (auto& item : *this)
            new (&item) value_type{} ;
      }

      void _destroy (void)
      {
         if (!m_data)
            return ;

         for (auto& item : *this)
            item.~value_type() ;

         free (m_data) ;
         m_data = nullptr ;
      }

   private:
      _Tp *m_data ;
      uint8_t m_size ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
