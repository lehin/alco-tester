////////////////////////////////////////////////////////////////////////////////////////////
//
// scheduler.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "scheduler.h"
#include "ll-scheduler.h"
#include <avr/io.h>
#include <util/atomic.h>

////////////////////////////////////////////////////////////////////////////////////////////

ISR (TIMER1_OVF_vect)
{
   aux::Scheduler::_on_overflow() ;
}

ISR (TIMER1_COMPA_vect)
{
   aux::Scheduler::_schedule_check_events() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

Scheduler::Event::Event (uint16_t id, uint16_t delay_ms, Slot slot) :
   m_id { id },
   m_slot { slot }
{
   // calculation are based on selected timer clock: 8MHz / 1024 (prescaler)
   // it gives 65531 ticks in 8388 ms and fits the 16-bit integer range

   auto cur = TCNT1 ;
   uint16_t tm = cur + (uint32_t)delay_ms * 65531 / 8388 ;

   m_time = tm ;
   m_cr = (tm < cur) || m_overflow_scheduled ;
}

////////////////////////////////////////////////////////////////////////////////////////////

List<Scheduler::Event> Scheduler::m_schedule ;
uint16_t Scheduler::m_schedule_id ;

volatile bool Scheduler::m_check_events_scheduled ;
volatile bool Scheduler::m_overflow_scheduled ;

SINGLETON_IMPL (Scheduler) ;

Scheduler::Scheduler (void)
{
   // prescaler = clk / 1024
   TCCR1B = _BV(CS10) | _BV (CS12) ;
   TIMSK1 = _BV(TOIE1) ;   // включить только прерывание по переполнению (OCR1A) таймера 1
}

uint16_t Scheduler::schedule (Slot slot, uint16_t delay_ms)
{
   Event e { _next_id(), delay_ms, forward (slot) } ;

   auto ins = m_schedule.begin() ;
   for (; ins != m_schedule.end() && *ins < e; ++ins) ;

   auto it = m_schedule.insert (ins, forward (e)) ;
   if (it == m_schedule.begin())
      _schedule_check_events() ;

   return e.m_id ;
}

void Scheduler::cancel (uint16_t schedule)
{
   bool first = false ;

   for (auto del = m_schedule.begin(); del != m_schedule.end(); ++del)
      if (del->m_id == schedule)
      {
         first = (del == m_schedule.begin()) ;
         m_schedule.erase (del) ;
         break ;
      }

   if (first)
      _schedule_check_events() ;
}

void Scheduler::_check_events (void)
{
   for (auto& e : _check_events_prepare())
      e.m_slot() ;
}

Scheduler::Events Scheduler::_check_events_prepare (void)
{
   m_check_events_scheduled = false ;

   auto last = m_schedule.begin() ;

   for (; last != m_schedule.end() && !last->m_cr; ++last)
   {
      if (last->m_time > TCNT1)
      {
         OCR1A = last->m_time ;
         TIFR1 = _BV(OCF1A) ;
      }

      if (last->m_time > TCNT1)
      {
         TIMSK1 |= _BV(OCIE1A) ;
         break ;
      }
   }

   auto res = m_schedule.pull (m_schedule.begin(), last) ;

   if (m_schedule.empty())
      TIMSK1 &= ~_BV(OCIE1A) ;

   return res ;
}

void Scheduler::_handle_overflow (void)
{
   m_overflow_scheduled = false ;

   for (auto& e: _handle_overflow_prepare())
      e.m_slot() ;
}

Scheduler::Events Scheduler::_handle_overflow_prepare (void)
{
   auto it = m_schedule.begin() ;
   for (; it != m_schedule.end() && !it->m_cr; ++it) ;

   auto res = m_schedule.pull (m_schedule.begin(), it) ;

   for (auto& e : m_schedule)
      e.m_cr = false ;

   _schedule_check_events() ;

   return res ;
}

inline
   void Scheduler::_schedule_check_events (void)
   {
      ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
      {
         if (m_check_events_scheduled)
            return ;

         m_check_events_scheduled = true ;
         TIMSK1 &= ~_BV(OCIE1A) ;
      }

      LowLatencySched::schedule (&Scheduler::_check_events) ;
   }

inline
   uint16_t Scheduler::_next_id (void)
   {
      while (!++m_schedule_id) ;
      return m_schedule_id ;
   }

inline
   void __attribute__ ((always_inline)) Scheduler::_on_overflow (void)
   {
      m_overflow_scheduled = true ;
      LowLatencySched::schedule (&Scheduler::_handle_overflow) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
