////////////////////////////////////////////////////////////////////////////////////////////
//
// list.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "forward.h"
#include <stddef.h>

namespace aux
{

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Tp>
   class List
   {
      typedef _Tp value_type ;

      struct Item
      {
         Item *m_next, *m_prev ;
         value_type m_value ;

         Item (const value_type& val) : m_value (val) {}
         Item (value_type&& val) : m_value (forward (val)) {}
         Item (void) {}
      } ;

   public:
      class iterator
      {
         friend class List ;
         Item *m_item ;

         iterator (Item *item) : m_item { item } {}

         Item* raw (void) const { return m_item ; }

      public:
         typedef value_type value ;
         typedef value* pointer ;
         typedef value& reference ;

      public:
         iterator (void) {}

         pointer operator-> (void) const { return &m_item->m_value ; }
         reference operator* (void) const { return m_item->m_value ; }

         iterator& operator++ (void) { m_item = m_item->m_next ; return *this ; }
         iterator& operator-- (void) { m_item = m_item->m_prev ; return *this ; }

         iterator operator++ (int) { iterator ret = *this ; ++*this ; return ret ; }
         iterator operator-- (int) { iterator ret = *this ; --*this ; return ret ; }

         friend inline bool operator== (const iterator& lhs, const iterator& rhs) { return lhs.m_item == rhs.m_item ; }
         friend inline bool operator!= (const iterator& lhs, const iterator& rhs) { return lhs.m_item != rhs.m_item ; }
      } ;

   private:
      List (const List&) = delete ;
      int operator= (const List&) = delete ;

      List (Item *head, Item *tail) ;

   public:
      List (void) ;
      List (List&& rhs) ;
      ~List (void) ;

      List& operator= (List&& rhs) ;

      iterator begin (void) const ;
      iterator end (void) const ;

      iterator find (const value_type& val) const ;

      template<typename _Fn>
         iterator find_if (const _Fn& fn) const ;

      iterator insert (const iterator& ins, const value_type& val) ;
      iterator insert (const iterator& ins, value_type&& val) ;

      void erase (const iterator& del) ;

      void push_front (const value_type& val) ;
      void push_front (value_type&& val) ;

      void push_back (const value_type& val) ;
      void push_back (value_type&& val) ;

      value_type& front (void) ;
      value_type& back (void) ;

      bool empty (void) const ;
      size_t size (void) const ;

      List pull (const iterator& first, const iterator& last) ;

      void clear (void) ;

   private:
      iterator _insert_impl (const iterator& ins, Item *item) ;
      void _destroy (void) ;

   private:
      Item *m_head ;
      Item m_tail ;
   } ;


template<typename _Tp> inline
   List<_Tp>::List (Item *head, Item *tail) :
      m_head { head }
   {
      m_tail.m_next = &m_tail ;
      m_tail.m_prev = tail ;

      head->m_prev = nullptr ;
      tail->m_next = &m_tail ;
   }

template<typename _Tp> inline
   List<_Tp>::List (void) :
      m_head { &m_tail }
   {
      m_tail.m_next = &m_tail ;
      m_tail.m_prev = nullptr ;
   }

template<typename _Tp> inline
   List<_Tp>::List (List&& rhs)
   {
      m_tail.m_next = &m_tail ;
      m_tail.m_prev = rhs.m_tail.m_prev ;

      if (!m_tail.m_prev)
         m_head = &m_tail ;
      else
      {
         m_head = rhs.m_head ;
         m_tail.m_prev->m_next = &m_tail ;
      }

      rhs.m_head = &rhs.m_tail ;
      rhs.m_tail.m_next = &rhs.m_tail ;
      rhs.m_tail.m_prev = nullptr ;
   }

template<typename _Tp> inline
   List<_Tp>::~List (void)
   {
      _destroy() ;
   }

template<typename _Tp> inline
   List<_Tp>& List<_Tp>::operator= (List&& rhs)
   {
      _destroy() ;

      m_tail.m_prev = rhs.m_tail.m_prev ;

      if (!m_tail.m_prev)
         m_head = &m_tail ;
      else
      {
         m_head = rhs.m_head ;
         m_tail.m_prev->m_next = &m_tail ;
      }

      rhs.m_head = &rhs.m_tail ;
      rhs.m_tail.m_next = &rhs.m_tail ;
      rhs.m_tail.m_prev = nullptr ;

      return *this ;
   }

template<typename _Tp> inline
   typename List<_Tp>::iterator List<_Tp>::begin (void) const
   {
      return m_head ;
   }

template<typename _Tp> inline
   typename List<_Tp>::iterator List<_Tp>::end (void) const
   {
      return const_cast<Item*> (&m_tail) ;
   }

template<typename _Tp>
   typename List<_Tp>::iterator List<_Tp>::find (const value_type& val) const
   {
      iterator res = begin() ;
      for (; res != end(); ++res)
         if (*res == val)
            break ;

      return res ;
   }

template<typename _Tp>
   template<typename _Fn>
      typename List<_Tp>::iterator List<_Tp>::find_if (const _Fn& fn) const
      {
         iterator res = begin() ;
         for (; res != end(); ++res)
            if (fn (*res))
               break ;

         return res ;
      }

template<typename _Tp> inline
   typename List<_Tp>::iterator List<_Tp>::insert (const iterator& ins, const value_type& val)
   {
      return _insert_impl (ins, new Item { val }) ;
   }

template<typename _Tp> inline
   typename List<_Tp>::iterator List<_Tp>::insert (const iterator& ins, value_type&& val)
   {
      return _insert_impl (ins, new Item { forward (val) }) ;
   }

template<typename _Tp>
   void List<_Tp>::erase (const iterator& del)
   {
      auto i = del.raw() ;
      if (i == &m_tail)
         return ;

      (i->m_prev ? i->m_prev->m_next : m_head) = i->m_next ;
      i->m_next->m_prev = i->m_prev ;

      delete i ;
   }

template<typename _Tp> inline
   void List<_Tp>::push_front (const value_type& val)
   {
      insert (begin(), val) ;
   }

template<typename _Tp> inline
   void List<_Tp>::push_back (const value_type& val)
   {
      insert (end(), val) ;
   }

template<typename _Tp> inline
   void List<_Tp>::push_front (value_type&& val)
   {
      insert (begin(), forward (val)) ;
   }

template<typename _Tp> inline
   void List<_Tp>::push_back (value_type&& val)
   {
      insert (end(), forward (val)) ;
   }

template<typename _Tp> inline
   typename List<_Tp>::value_type& List<_Tp>::front (void)
   {
      return m_head->m_value ;
   }

template<typename _Tp> inline
   typename List<_Tp>::value_type& List<_Tp>::back (void)
   {
      return m_tail.m_prev->m_value ;
   }

template<typename _Tp> inline
   bool List<_Tp>::empty (void) const
   {
      return begin() == end() ;
   }


template<typename _Tp>
   size_t List<_Tp>::size (void) const
   {
      size_t ret = 0 ;
      for (auto it = begin(); it != end(); ++it, ++ret) ;
      return ret ;
   }

template<typename _Tp>
   List<_Tp> List<_Tp>::pull (const iterator& first, const iterator& last)
   {
      if (first == last)
         return {} ;

      auto head = first.raw(),
           tail = last.raw(),
           back = tail->m_prev ;

      (head->m_prev ? head->m_prev->m_next : m_head) = tail ;
      tail->m_prev = head->m_prev ;

      return { head, back } ;
   }

template<typename _Tp>
   void List<_Tp>::clear (void)
   {
      _destroy() ;
   }

template<typename _Tp>
   typename List<_Tp>::iterator List<_Tp>::_insert_impl (const iterator& ins, Item *item)
   {
      item->m_prev = ins.raw()->m_prev ;
      item->m_next = ins.raw() ;

      ins.raw()->m_prev = item ;
      (item->m_prev ? item->m_prev->m_next : m_head) = item ;

      return item ;
   }

template<typename _Tp>
   void List<_Tp>::_destroy (void)
   {
      while (!empty())
      {
         auto del = m_head ;
         m_head = m_head->m_next ;
         delete del ;
      }

      m_tail.m_prev = nullptr ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace aux
