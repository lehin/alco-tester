////////////////////////////////////////////////////////////////////////////////////////////
//
// item-sensor.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "item-sensor.h"
#include "peripheral/display.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

void ItemSensor::do_draw (float val) const
{
   uint8_t x = 40 ;

   bool dot = false ;
   float div = 1 ;
   while (val / div >= 10) div *= 10 ;

   for (uint8_t dn = 0; dn < 4; ++dn)
   {
      if (div < 1 && !dot)
      {
         dot = true ;
         peripheral::Display::putc (x, 3, '.') ;
         x += 6 ;
      }

      uint8_t d = val / div ;

      peripheral::Display::putc (x, 3, '0' + d) ;
      x += 6 ;

      val -= d * div ;
      div /= 10 ;
   }
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
