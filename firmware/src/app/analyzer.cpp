////////////////////////////////////////////////////////////////////////////////////////////
//
// analyzer.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "analyzer.h"
#include "peripheral/gas-sensor.h"
#include "app/screen.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Analyzer ANALYZER ;

////////////////////////////////////////////////////////////////////////////////////////////

void Analyzer::reset (void)
{
   m_buffer.clear() ;
   m_derivative = 0 ;
   m_stable = true ;
   peripheral::GAS.on_resistance ([] (auto r) { ANALYZER._on_resistance (r) ; }) ;
}

void Analyzer::_on_resistance (float r)
{
   if (m_buffer.full())
      m_buffer.pop_front() ;
   else if (m_buffer.size() < 10)
      m_last = r ;

   m_buffer.push_back (r) ;

   m_derivative -= m_derivative / 10 ;
   m_derivative += r - m_last ;
   m_last = r ;

   auto normalized = m_derivative / r * 100 ;

   static constexpr float STABLE_LEVEL = 0.2 ;

   #ifndef CALIBRATION
   static constexpr float UNSTABLE_LEVEL = 3 ;
   #else
   static constexpr float UNSTABLE_LEVEL = 0.5 ;
   #endif

   bool stable = fabs (normalized) < (m_stable ? 3 : 0.2) ;
   if (!stable)
      _set_unstable (normalized < 0) ;
   else
      _check_stable() ;

   auto sign = _qualify (m_derivative) ;

   _check_extremum (m_last_sign, sign) ;

   if (sign != FSign::Zero)
      m_last_sign = sign ;

   app::SCREEN.set_sensor (r) ;
}

void Analyzer::_set_unstable (bool decreasing)
{
   m_stability = 0 ;

   if (!m_stable)
      return ;

   _set_stability (false, decreasing) ;
}

void Analyzer::_check_stable (void)
{
   static constexpr uint8_t STABILITY_PERIOD = 20 ;

   if (m_stable || ++m_stability < STABILITY_PERIOD)
      return ;

   _set_stability (true, false) ;
}

void Analyzer::_set_stability (bool stable, bool decreasing)
{
   m_stable = stable ;

   if (m_on_stability)
      m_on_stability (stable, decreasing) ;
}

void Analyzer::_check_extremum (FSign prev, FSign cur)
{
   if (prev == FSign::Zero || cur == FSign::Zero || prev == cur)
      return ;

   bool minimum = prev == FSign::Negative && cur == FSign::Positive ;

   float val = m_buffer[0] ;

   for (auto r : m_buffer)
      if (minimum)
      {
         if (r < val)
            val = r ;
      }
      else
      {
         if (r > val)
            val = r ;
      }

   if (m_on_extremum)
      m_on_extremum (minimum, val) ;
}

Analyzer::FSign Analyzer::_qualify (float val)
{
   if (fabs(val) < 1e-3)
      return FSign::Zero ;

   return val < 0 ? FSign::Negative : FSign::Positive ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
