////////////////////////////////////////////////////////////////////////////////////////////
//
// item-battery.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include <stdint.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

static constexpr uint8_t BATTERY_SIZE_PX = 30 ;

////////////////////////////////////////////////////////////////////////////////////////////

class BatteryVal
{
   static constexpr uint16_t VMAX = 405 ;
   static constexpr uint16_t VMIN = 320 ;

public:
   BatteryVal (uint16_t voltage)
   {
      if (voltage >= VMAX)
         m_val = BATTERY_SIZE_PX ;
      else if (voltage <= VMIN)
         m_val = 0 ;
      else
         m_val = (voltage - VMIN) * BATTERY_SIZE_PX / (VMAX - VMIN) ;
   }

   operator uint8_t (void) const { return m_val ; }

   inline friend bool operator== (const BatteryVal& lhs, const BatteryVal& rhs) { return lhs.m_val == rhs.m_val ; }

private:
   uint8_t m_val ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class ItemBattery : public ScreenItem<ItemBattery,BatteryVal>
{
public:
   ItemBattery (void) : base_class{ 420u } {}

   void do_draw (uint8_t val) const ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
