////////////////////////////////////////////////////////////////////////////////////////////
//
// power-ctrl.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "power-ctrl.h"
#include "screen.h"
#include "tester.h"
#include "aux/scheduler.h"
#include "hw/power.h"
#include "hw/led.h"
#include "peripheral/battery.h"
#include "peripheral/gas-sensor.h"
#include "peripheral/ext-signals.h"
#include "peripheral/beeper.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

PowerCtrl POWER ;

////////////////////////////////////////////////////////////////////////////////////////////

void PowerCtrl::start (void)
{
   hw::POWER.on_run ([] { POWER._on_run() ; }) ;
   hw::POWER.on_stop ([] { POWER._on_stop() ; }) ;

   peripheral::BAT.on_lowbat ([] { POWER._on_lowbat() ; }) ;
   peripheral::EXTSIGNALS.on_charger ([] (bool c) { POWER._on_charger (c) ; }) ;
   peripheral::EXTSIGNALS.on_charging ([] (bool c) { POWER._on_charging (c) ; }) ;

   peripheral::EXTSIGNALS.on_pending
      (
         [] (bool pending)
         {
            if (pending)
               POWER.lock() ;
            else
               POWER.unlock() ;
         }
      ) ;

   sleep() ;
}

void PowerCtrl::sleep (void)
{
   hw::POWER.stop() ;
}

void PowerCtrl::off (void)
{
   if (!m_on)
      return ;

   m_on = false ;

   _cancel_poweroff_timer() ;
   m_poweroff_locks = 0 ;

   app::TESTER.shutdown() ;
   peripheral::GAS.shutdown() ;
   peripheral::BAT.shutdown() ;
   app::SCREEN.shutdown() ;
   hw::POWER.load_off() ;
}

void PowerCtrl::lock (void)
{
   #ifndef CALIBRATION
   if (!m_on)
      return ;

   if (!m_poweroff_locks)
      _cancel_poweroff_timer() ;

   ++m_poweroff_locks ;
   #endif
}

void PowerCtrl::unlock (void)
{
   #ifndef CALIBRATION
   if (!m_on || --m_poweroff_locks)
      return ;

   m_poweroff_timeout = 120 ;
   _schedule_poweroff() ;
   #endif
}

void PowerCtrl::_on_run (void)
{
   if (m_on || peripheral::EXTSIGNALS.charger())
      return ;

   m_on = true ;
   m_poweroff_locks = 0 ;

   hw::POWER.battery_on() ;
   hw::POWER.load_on() ;

   app::SCREEN.initialize() ;
   app::TESTER.reset() ;

   peripheral::GAS.initialize() ;
   peripheral::BAT.initialize() ;
}

void PowerCtrl::_on_stop (void)
{
   peripheral::EXTSIGNALS.lock_charging() ;
   peripheral::EXTSIGNALS.unlock_button() ;

   off() ;
}

void PowerCtrl::_on_lowbat (void)
{
   off() ;

   peripheral::Beeper::beep (peripheral::Beeper::Type::On) ;

   aux::Scheduler::schedule
      (
         []
         {
            peripheral::Beeper::mute() ;
            POWER.sleep() ;
         },
         1000
      ) ;
}

void PowerCtrl::_on_charger (bool charger)
{
   if (charger)
   {
      off() ;
      hw::LED.on() ;
      peripheral::EXTSIGNALS.unlock_charging() ;
      peripheral::EXTSIGNALS.lock_button() ;
   }
   else
   {
      hw::LED.off() ;
      sleep() ;
   }
}

void PowerCtrl::_on_charging (bool charging)
{
   if (charging)
   {
      hw::LED.off() ;
      hw::LED.blink (1000, 1000) ;
   }
   else
      hw::LED.on() ;
}

void PowerCtrl::_on_poweroff_timer (void)
{
   if (!m_poweroff_schedule)
      return ;

   m_poweroff_schedule = 0 ;

   if (!m_poweroff_timeout || !--m_poweroff_timeout)
      sleep() ;
   else
      _schedule_poweroff() ;
}

void PowerCtrl::_schedule_poweroff (void)
{
   if (m_poweroff_schedule)
      return ;

   m_poweroff_schedule = aux::Scheduler::schedule ([] { POWER._on_poweroff_timer() ; }, 1000) ;
}

void PowerCtrl::_cancel_poweroff_timer (void)
{
   if (!m_poweroff_schedule)
      return ;

   aux::Scheduler::cancel (m_poweroff_schedule) ;
   m_poweroff_schedule = 0 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
