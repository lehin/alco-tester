////////////////////////////////////////////////////////////////////////////////////////////
//
// item-permile.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "screen-item.h"
#include "tester.h"
#include <math.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class ItemTesterState
{
   using State = Tester::State ;
   using Units = Tester::Units ;

public:
   ItemTesterState (State state = State::Reset, Units units = Units::MgLiter, float value = NAN) :
      m_state{ state }, m_units{ units }, m_value{ value } {}

   State state (void) const { return m_state ; }
   Units units (void) const { return m_units ; }
   float value (void) const { return m_value ; }

   inline friend bool operator== (const ItemTesterState& lhs, const ItemTesterState& rhs)
      { return lhs.m_state == rhs.m_state && lhs.m_units == rhs.m_units && lhs.m_value == rhs.m_value ; }

private:
   State m_state ;
   Units m_units ;
   float m_value ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

class ItemPermille : public ScreenItem<ItemPermille,ItemTesterState>
{
   typedef char String[6] ;

public:
   void do_draw (const ItemTesterState& state) ;

private:
   static void _format_value (String s, double val, Tester::Units units) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
