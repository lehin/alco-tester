////////////////////////////////////////////////////////////////////////////////////////////
//
// analyzer.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/ring-buffer.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class Analyzer
{
   enum class FSign : uint8_t{ Zero, Positive, Negative } ;

public:
   void reset (void) ;

   typedef void (*OnStability) (bool stable, bool decreasing) ;
   void on_stability (OnStability fn) { m_on_stability = fn ; }

   typedef void (*OnExtremum) (bool minimum, float resistance) ;
   void on_extremum (OnExtremum fn) { m_on_extremum = fn ; }

private:
   void _on_resistance (float r) ;
   void _set_unstable (bool decreasing) ;
   void _check_stable (void) ;
   void _set_stability (bool stable, bool decreasing) ;
   void _check_extremum (FSign prev, FSign cur) ;

   static FSign _qualify (float val) ;

private:
   aux::RingBuffer<40,float> m_buffer ;

   float m_last = 0,
         m_derivative = 0 ;

   FSign m_last_sign = FSign::Zero ;

   bool m_stable ;

   uint8_t m_stability ;

   OnStability m_on_stability = nullptr ;
   OnExtremum m_on_extremum = nullptr ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern Analyzer ANALYZER ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
