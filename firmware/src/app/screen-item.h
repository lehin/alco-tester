////////////////////////////////////////////////////////////////////////////////////////////
//
// screen-item.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class ScreenItemBase
{
public:
   void reset (void) { set_dirty() ; }

protected:
   bool is_dirty (void) const { return m_dirty ; }
   void set_dirty (bool dirty = true) { m_dirty = dirty ; }

private:
   bool m_dirty = true ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _Result, typename _Tp>
   class ScreenItem : protected ScreenItemBase
   {
   public:
      typedef _Tp value_type ;
      typedef ScreenItem base_class ;

      using ScreenItemBase::reset ;

   public:
      template<typename ... _Args>
         ScreenItem (_Args ... args) : m_value{ args... } {}

      template<typename ... _Args>
         void update (_Args ... args)
         {
            value_type v { args... } ;

            if (v == m_value)
               return ;

            m_value = v ;
            set_dirty() ;
         }

      void draw (void)
      {
         if (!is_dirty())
            return ;

         set_dirty (false) ;

         static_cast<_Result*>(this) -> do_draw (m_value) ;
      }

   private:
      value_type m_value ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
