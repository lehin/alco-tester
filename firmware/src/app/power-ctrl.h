////////////////////////////////////////////////////////////////////////////////////////////
//
// power-ctrl.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class PowerCtrl
{
public:
   void start (void) ;
   void sleep (void) ;
   void off (void) ;

   void lock (void) ;
   void unlock (void) ;

private:
   void _on_run (void) ;
   void _on_stop (void) ;
   void _on_lowbat (void) ;
   void _on_charger (bool charger) ;
   void _on_charging (bool charging) ;

   void _on_poweroff_timer (void) ;
   void _schedule_poweroff (void) ;
   void _cancel_poweroff_timer (void) ;

private:
   bool m_on = false ;

   uint8_t m_poweroff_timeout ;
   uint16_t m_poweroff_schedule = 0 ;
   uint8_t m_poweroff_locks = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern PowerCtrl POWER ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
