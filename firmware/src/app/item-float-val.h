////////////////////////////////////////////////////////////////////////////////////////////
//
// item-float-val.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <math.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class ItemFloatVal
{
public:
   ItemFloatVal (float v) : m_v{ v } {}

   operator float (void) const { return m_v ; }

   inline friend bool operator== (const ItemFloatVal& lhs, const ItemFloatVal& rhs)
      { return fabs (lhs - rhs) < 0.001 ; }

private:
   float m_v ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
