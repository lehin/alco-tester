////////////////////////////////////////////////////////////////////////////////////////////
//
// screen.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "screen.h"
#include "aux/scheduler.h"
#include "peripheral/display.h"

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Screen SCREEN ;

////////////////////////////////////////////////////////////////////////////////////////////

void Screen::initialize (void)
{
   peripheral::Display::initialize() ;

   m_range.reset() ;
   m_sensor.reset() ;
   m_permille.reset() ;
   m_battery.reset() ;
   m_ready.reset() ;

   _schedule_update() ;
}

void Screen::shutdown (void)
{
   aux::Scheduler::cancel (m_update_schedule) ;
   m_update_schedule = 0 ;
}

void Screen::_schedule_update (void)
{
   m_update_schedule =
      aux::Scheduler::schedule
         (
            []
            {
               if (!SCREEN.m_update_schedule)
                  return ;

               SCREEN._schedule_update() ;
               SCREEN._update() ;
            },
            100
         ) ;
}

void Screen::_update (void)
{
   m_range.draw() ;
   m_sensor.draw() ;
   m_permille.draw() ;
   m_battery.draw() ;
   m_ready.draw() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
