////////////////////////////////////////////////////////////////////////////////////////////
//
// item-permile.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "item-permile.h"
#include "peripheral/display.h"
#include "aux/scheduler.h"
#include <string.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

void ItemPermille::do_draw (const ItemTesterState& state)
{
   String out{ 0x0a, 0x0a, 0x0a, 0x0a, 0x0a, 0x0a } ;

   switch (state.state())
   {
      case Tester::State::Reset: out[0] = 14 ; break ;

      #ifdef CALIBRATION
      case Tester::State::Measuring:
      #endif

      case Tester::State::Measured:
      case Tester::State::Ready:
         _format_value (out, state.value(), state.units()) ;
         break ;

      #ifndef CALIBRATION
      case Tester::State::Measuring:
         memset (out, 16, sizeof (out)) ;
         break ;
      #endif
   }

   uint8_t x = 0 ;
   for (auto sym : out)
   {
      peripheral::Display::digit (x, 0, sym) ;
      x += 16 ;
   }
}

void ItemPermille::_format_value (String s, double val, Tester::Units units)
{
   bool _isnan = isnan (val) ;
   bool _isvalid = !_isnan && val >= 0.9 ;

   if (!_isvalid)
      memset (s, _isnan ? 12 : 16, sizeof (String) - 1) ;
   else
   {
      #ifndef CALIBRATION

      static constexpr double A = 3.570695344231413 ;
      static constexpr double B = -1.681818675906199 ;
      static constexpr double C = 0.9856117019259585 ;

      val = A / (val * val + B * val + C) ;
      if (val < 0)
         val = 0 ;

      static constexpr double PERMILLE_MULT = 20.0/9 ;

      if (units == Tester::Units::Permille)
         val *= PERMILLE_MULT ;

      #endif

      char *p = s ;

      bool dot = false ;
      float div = 1 ;
      while (val / div >= 10) div *= 10 ;

      for (uint8_t dn = 0; dn < 4; ++dn)
      {
         if (div < 1 && !dot)
         {
            dot = true ;
            *p++ = 11 ;
         }

         uint8_t digit = val / div ;
         *p++ = digit ;
         val -= digit * div ;
         div /= 10 ;
      }
   }

   s[sizeof (String) - 1] = (units == Tester::Units::Permille ? 13 : 17) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
