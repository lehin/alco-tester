////////////////////////////////////////////////////////////////////////////////////////////
//
// screen.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "item-permile.h"
#include "item-battery.h"
#include "item-range.h"
#include "item-sensor.h"
#include "item-ready.h"
#include <stdint.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class Screen
{
public:
   void initialize (void) ;
   void shutdown (void) ;

   void set_range (uint8_t range) { m_range.update (range) ; }
   void set_sensor (float val) { m_sensor.update (val) ; }
   void set_battery (uint16_t voltage) { m_battery.update (voltage) ; }
   void set_ready (bool ready) { m_ready.update (ready) ; }

   template<typename ... _Args>
      void set_permille (_Args ... args) { m_permille.update (args...) ; }

private:
   void _schedule_update (void) ;
   void _update (void) ;

private:
   uint16_t m_update_schedule ;

   ItemRange m_range ;
   ItemSensor m_sensor ;
   ItemPermille m_permille ;
   ItemBattery m_battery ;
   ItemReady m_ready ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern Screen SCREEN ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
