////////////////////////////////////////////////////////////////////////////////////////////
//
// tester.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "tester.h"
#include "analyzer.h"
#include "screen.h"
#include "power-ctrl.h"
#include "peripheral/beeper.h"
#include "peripheral/ext-signals.h"
#include "aux/scheduler.h"
#include <math.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

Tester TESTER ;

////////////////////////////////////////////////////////////////////////////////////////////

void Tester::start (void)
{
   ANALYZER.on_stability ([] (bool stable, bool dec) { TESTER._on_stability (stable, dec) ; }) ;
   ANALYZER.on_extremum ([] (bool minimal, float resistance) { TESTER._on_extremum (minimal, resistance) ; }) ;

   peripheral::EXTSIGNALS.on_button ([] (bool p, bool l) { TESTER._on_button (p, l) ; }) ;
   peripheral::EXTSIGNALS.enable() ;

   POWER.start() ;
}

void Tester::reset (void)
{
   ANALYZER.reset() ;

   _set_state (State::Reset) ;
   _schedule_wait_unstable() ;
}

void Tester::_set_state (State state)
{
   m_state = state ;

   switch (m_state)
   {
      case State::Measuring:
         peripheral::Beeper::beep (peripheral::Beeper::Type::Single) ;

      case State::Reset:
         m_tested = NAN ;
         POWER.lock() ;
         break ;

      case State::Ready:
         POWER.unlock() ;
         peripheral::Beeper::beep (peripheral::Beeper::Type::Triple) ;
         break ;

      case State::Measured:
         peripheral::Beeper::beep (peripheral::Beeper::Type::Double) ;
         break ;
   } ;

   _update_screen() ;
}

void Tester::_update_screen (void)
{
   app::SCREEN.set_permille (m_state, m_units, m_tested) ;
   app::SCREEN.set_ready (m_state == State::Ready) ;
}

void Tester::_on_stability (bool stable, bool decreasing)
{
   _cancel_wait_unstable() ;

   if (stable)
   {
      if (m_state != State::Measuring)
      #ifndef CALIBRATION
         _set_state (State::Ready) ;
      #else
         _set_state (State::Measuring) ;
      #endif
   }
   else if (m_state == State::Ready)
      _set_state (decreasing ? State::Measuring : State::Reset) ;
}

void Tester::_on_extremum (bool minimal, float resistance)
{
   if (m_state != State::Measuring || !minimal)
      return ;

   #ifdef CALIBRATION
   if (!isnan (m_tested) && m_tested <= resistance)
      return ;
   #endif

   m_tested = resistance ;

   #ifndef CALIBRATION
   _set_state (State::Measured) ;
   #else
   peripheral::Beeper::beep (peripheral::Beeper::Type::Double) ;
   _update_screen() ;
   #endif
}

void Tester::_on_button (bool pressed, bool longpress)
{
   if (!longpress)
   {
      if (pressed || (m_state != State::Ready && m_state != State::Measured))
         return ;

      m_units = (m_units == Units::Permille ? Units::MgLiter : Units::Permille) ;
      _update_screen() ;
   }
   else
      if (pressed)
         POWER.off() ;
      else
         POWER.sleep() ;
}

void Tester::_schedule_wait_unstable (void)
{
   m_wait_unstable_schedule =
      aux::Scheduler::schedule
         (
            []
            {
               if (!TESTER.m_wait_unstable_schedule)
                  return ;

               TESTER.m_wait_unstable_schedule = 0 ;
               TESTER._on_stability (true, false) ;
            },
            8000
         ) ;
}

void Tester::_cancel_wait_unstable (void)
{
   if (!m_wait_unstable_schedule)
      return ;

   aux::Scheduler::cancel (m_wait_unstable_schedule) ;
   m_wait_unstable_schedule = 0 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
