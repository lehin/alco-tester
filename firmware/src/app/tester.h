////////////////////////////////////////////////////////////////////////////////////////////
//
// tester.h
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdint.h>

namespace app
{

////////////////////////////////////////////////////////////////////////////////////////////

class Tester
{
public:
   enum class State : uint8_t { Reset, Ready, Measuring, Measured } ;
   enum class Units : uint8_t { MgLiter, Permille, _Undefined = 255 } ;

public:
   void start (void) ;
   void reset (void) ;
   void shutdown (void) { _cancel_wait_unstable() ; }

private:
   void _set_state (State state) ;
   void _update_screen (void) ;
   void _on_stability (bool stable, bool decreasing) ;
   void _on_extremum (bool minimal, float resistance) ;
   void _on_button (bool pressed, bool longpress) ;
   void _schedule_wait_unstable (void) ;
   void _cancel_wait_unstable (void) ;

private:
   State m_state = State::Reset ;
   Units m_units = Units::MgLiter ;
   float m_tested ;

   uint16_t m_wait_unstable_schedule = 0 ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

extern Tester TESTER ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace app
