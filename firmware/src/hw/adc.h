////////////////////////////////////////////////////////////////////////////////////////////
//
// adc.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include "aux/ring-buffer.h"
#include <avr/interrupt.h>

ISR (ADC_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class Adc
{
   SINGLETON_DECL (Adc) ;

public:
   enum Vref : uint8_t
   {
      Aref = 0,
      Avcc = _BV(REFS0),
      Int = _BV(REFS0) | _BV(REFS1)
   } ;

public:
   Adc (void) ;

   typedef void (*DataSink) (uint16_t volts) ;
   static void measure (uint8_t input, Vref vref, DataSink sink) ;

private:
   static void _check_pendings (void) ;
   static void _on_value_measured (void) ;

   friend void ::ADC_vect (void) ;
   static void _on_value_measured_interrupt (void) ;

private:
   static int8_t m_measured_count ;
   static uint16_t m_measured_sum ;
   static uint8_t m_last_admux ;

   struct Request
   {
      uint8_t m_admux ;
      DataSink m_sink ;
   } ;

   typedef aux::RingBuffer<4,Request> Pendings ;
   static Pendings m_pendings ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
