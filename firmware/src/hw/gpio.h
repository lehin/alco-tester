////////////////////////////////////////////////////////////////////////////////////////////
//
// gpio.h
//
// Copyright 2015 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/singleton.h"
#include <avr/io.h>

namespace hw
{
   namespace gpio
   {

////////////////////////////////////////////////////////////////////////////////////////////

struct Init
{
   SINGLETON_DECL (Init) ;
   Init (void) ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

#define PORTDEF(name) \
   struct Portdef##name \
   { \
      static volatile uint8_t& in (void) { return PIN##name ; } \
      static volatile uint8_t& out (void) { return PORT##name ; } \
      static volatile uint8_t& ddr (void) { return DDR##name ; } \
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

enum class Dir { In, Out } ;

template<int _num, Dir _dir = Dir::In, int _init = 1>
   struct PinD
   {
      static constexpr uint8_t mask (void) { return _BV(_num) ; }
      static constexpr uint8_t ddr (void) { return _dir == Dir::Out ? mask() : 0 ; }
      static constexpr uint8_t init (void) { return _init ? mask() : 0 ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template<typename _PinD = void, typename ... _Tail>
   struct PortPins : PortPins<_Tail...>
   {
      typedef PortPins<_Tail...> base_class ;

      static constexpr uint8_t ddr (void)
      {
         return (base_class::ddr() & ~_PinD::mask()) | _PinD::ddr() ;
      }

      static constexpr uint8_t init (void)
      {
         return (base_class::init() & ~_PinD::mask()) | _PinD::init() ;
      }
   } ;

template<>
   struct PortPins<void>
   {
      static constexpr uint8_t ddr (void) { return 0 ; }
      static constexpr uint8_t init (void) { return 0xFF ; }
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

template< typename _PortDef, typename ... _PinsDef >
   class Port : public _PortDef
   {
   public:
      using _PortDef::in ;
      using _PortDef::out ;
      using _PortDef::ddr ;

      static void init (void)
      {
         out() = PortPins<_PinsDef...>::init() ;
         ddr() = PortPins<_PinsDef...>::ddr() ;
      }

      template<int _num>
         struct Pin
         {
            typedef Port Owner ;
            static constexpr int num = _num ;
            static constexpr uint8_t mask = _BV(num) ;

            template<int _set>
               static void set (void)
               {
                  if (_set)
                     Owner::out() |= mask ;
                  else
                     Owner::out() &= ~mask ;
               }

            static void set (void) { set<1>() ; }
            static void reset (void) { set<0>() ; }
            static void turn (void) { Owner::out() ^= _BV(num) ; }
            static bool read (void) { return (Owner::in() & _BV(num)) != 0 ; }
            static bool stat (void) { return (Owner::out() & _BV(num)) != 0 ; }

            static void as_input (void) { Owner::ddr() &= ~PinD<num>::mask() ; }
            static void as_output (void) { Owner::ddr() |= PinD<num>::mask() ; }
         } ;
   } ;

////////////////////////////////////////////////////////////////////////////////////////////

PORTDEF (B) ;
PORTDEF (C) ;
PORTDEF (D) ;

struct PortB :
   Port
      <
         PortdefB,
         PinD<0, Dir::Out, 0>,
         PinD<7, Dir::Out, 1>
      >
{
} ;

struct PortC :
   Port
      <
         PortdefC,
         PinD<0, Dir::In, 0>,
         PinD<1, Dir::In, 1>,
         PinD<4, Dir::Out, 0>,
         PinD<5, Dir::Out, 0>,
         PinD<7, Dir::In, 0>
      >
{
} ;


struct PortD :
   Port
      <
         PortdefD,
         PinD<0, Dir::Out, 0>,
         PinD<1, Dir::Out, 1>,
         PinD<2, Dir::In, 1>,
         PinD<3, Dir::In, 0>,
         PinD<4, Dir::Out, 1>,
         PinD<5, Dir::Out, 0>
      >
{
} ;

////////////////////////////////////////////////////////////////////////////////////////////

typedef PortB::Pin<0> Led ;
typedef PortB::Pin<7> Drst ;

typedef PortC::Pin<0> Vbat ;
typedef PortC::Pin<1> Chrg ;
typedef PortC::Pin<7> Sens ;

typedef PortD::Pin<0> Kamp ;
typedef PortD::Pin<1> Pwr ;
typedef PortD::Pin<2> Btn ;
typedef PortD::Pin<3> Vchrg ;
typedef PortD::Pin<4> On ;
typedef PortD::Pin<5> Beep ;

////////////////////////////////////////////////////////////////////////////////////////////

   } // namespace gpio
} // namespace hw
