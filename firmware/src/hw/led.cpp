////////////////////////////////////////////////////////////////////////////////////////////
//
// led.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "led.h"
#include "gpio.h"
#include "aux/scheduler.h"
#include <util/atomic.h>

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Led LED ;

////////////////////////////////////////////////////////////////////////////////////////////

void Led::turn (void)
{
   _cancel() ;

   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      gpio::Led::turn() ;
   }
}

void Led::blink (uint16_t on, uint16_t off)
{
   _cancel() ;

   m_on_time = on ;
   m_off_time = off ;

   _switch() ;
}

void Led::_on_off (bool on)
{
   if (on)
      gpio::Led::set() ;
   else
      gpio::Led::reset() ;
}

void Led::_cancel (void)
{
   if (!m_schedule)
      return ;

   aux::Scheduler::cancel (m_schedule) ;
   m_schedule = m_on_time = m_off_time = 0 ;
}

void Led::_switch (void)
{
   if (!m_on_time || !m_off_time)
      return ;

   gpio::Led::turn() ;
   m_schedule =
      aux::Scheduler::schedule
         (
            [] { LED._switch() ; },
            gpio::Led::stat() ? m_on_time : m_off_time
         ) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
