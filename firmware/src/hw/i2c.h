////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c.h
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#pragma once

#include "aux/array.h"
#include "aux/range.h"
#include "aux/debug-assert.h"
#include "aux/singleton.h"
#include <avr/interrupt.h>

ISR (TWI_vect) ;

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

class I2c
{
   static constexpr uint8_t BUFSIZES = 80 ;
   typedef aux::Array<uint8_t,BUFSIZES> IoBuffer ;

   SINGLETON_DECL (I2c) ;

public:
   class Tx
   {
      friend class I2c ;
      Tx (IoBuffer& buf) : m_buf (buf) { m_buf.clear() ; }

   public:
      template<typename  _Tp>
         Tx& operator<< (const _Tp& p) ;

      template<typename  _It>
         Tx& operator<< (const aux::Range<_It>& r) ;

      template<typename _Tp, typename ... _Args>
         void write (_Tp& p, _Args& ... tail) ;

      void write (void) {}

      bool empty (void) const { return m_buf.empty() ; }

   private:
      IoBuffer& m_buf ;
   } ;

   class Rx
   {
      friend class I2c ;
      Rx (IoBuffer& buf) : m_buf (buf), m_it (buf.begin()) {}

      struct Reader
      {
         Rx& m_this ;
         Reader (Rx& this_) : m_this (this_) {}

         template<typename _Tp>
            operator _Tp (void) const
               { return m_this.read<_Tp>() ; }
      } ;

   public:
      template<typename  _Tp>
         Rx& operator>> (_Tp& p) ;

      template<typename  _It>
         Rx& operator>> (const aux::Range<_It>& r) ;

      template<typename  _It>
         Rx& operator>> (aux::Range<_It>& r) ;

      Reader read (void) { return *this ; }

      template<typename _Tp>
         _Tp read (void) ;

   private:
      template<typename  _Range>
         void _read_range (const _Range& r) ;

   private:
      const IoBuffer& m_buf ;
      IoBuffer::iterator m_it ;
   } ;

   friend class Tx ;

public:
   I2c (void) ;

   static Tx tx (void) ;
   static Rx rx (void) { return m_io_buffer ; }
   static uint8_t receive (uint8_t addr, uint8_t sz) ;
   static uint8_t send (uint8_t addr) ;

private:
   static void _complete_request (uint8_t errno) ;

   friend void ::TWI_vect (void) ;
   static void _on_twi_event (void) ;

private:
   static uint8_t m_address ;
   static uint8_t m_retries ;
   static IoBuffer m_io_buffer ;
   static uint8_t m_io_pos ;

   static volatile uint8_t m_errno ;
   static volatile bool m_sending ;
} ;

////////////////////////////////////////////////////////////////////////////////////////////

namespace impl
{
   template<typename _Buf, typename _Tp, uint8_t _Sz>
      struct BufferPush
      {
         static void do_push (_Buf& buf, const _Tp& p)
         {
            aux::debug_assert( sizeof (_Tp) <= buf.capacity() - buf.size() ) ;

            reinterpret_cast<_Tp&> (*buf.end()) = p ;
            buf.resize (buf.size() + _Sz) ;
         }
      } ;

   template<typename _Buf, typename _Tp>
      struct BufferPush<_Buf,_Tp,1>
      {
         static void do_push (_Buf& buf, const _Tp& p)
         {
            aux::debug_assert( !buf.full() ) ;
            buf.push_back ((uint8_t)p) ;
         }
      } ;
}

////////////////////////////////////////////////////////////////////////////////////////////

template<typename  _Tp> inline
   I2c::Tx& I2c::Tx::operator<< (const _Tp& p)
   {
      impl::BufferPush<IoBuffer,_Tp,sizeof(_Tp)>::do_push (m_buf, p) ;
      return *this ;
   }

template<typename  _It> inline
   I2c::Tx& I2c::Tx::operator<< (const aux::Range<_It>& r)
   {
      for (auto& item : r)
         operator<< (item) ;

      return *this ;
   }

template<typename _Tp, typename ... _Args> inline
   void I2c::Tx::write (_Tp& p, _Args& ... tail)
   {
      operator<< (p) ;
      write (tail...) ;
   }


////////////////////////////////////////////////////////////////////////////////////////////

template<typename  _Tp> inline
   I2c::Rx& I2c::Rx::operator>> (_Tp& p)
   {
      aux::debug_assert( m_it + sizeof (_Tp) <= m_buf.end() ) ;

      p = reinterpret_cast<const _Tp&> (*m_it) ;
      m_it += sizeof (_Tp) ;

      return *this ;
   }

template<typename  _It> inline
   I2c::Rx& I2c::Rx::operator>> (const aux::Range<_It>& r)
   {
      _read_range (r) ;
      return *this ;
   }

template<typename  _It> inline
   I2c::Rx& I2c::Rx::operator>> (aux::Range<_It>& r)
   {
      _read_range (r) ;
      return *this ;
   }

template<typename _Tp> inline
   _Tp I2c::Rx::read (void)
   {
      _Tp res ;
      operator>> (res) ;
      return res ;
   }

template<typename  _Range> inline
   void I2c::Rx::_read_range (const _Range& r)
   {
      for (auto& item : r)
         operator>> (item) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
