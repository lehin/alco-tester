////////////////////////////////////////////////////////////////////////////////////////////
//
// power.cpp
//
// Copyright © 2019 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "power.h"
#include "gpio.h"
#include "adc.h"
#include "i2c.h"
#include "aux/ll-scheduler.h"
#include "aux/scheduler.h"
#include <avr/sleep.h>
#include <avr/power.h>
#include <util/atomic.h>

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

Power POWER ;

////////////////////////////////////////////////////////////////////////////////////////////

void Power::run (void)
{
   if (m_running)
      return ;

   m_running = true ;

   power_adc_enable() ;
   power_twi_enable() ;
   power_timer0_enable() ;
   power_timer1_enable() ;

   set_sleep_mode (SLEEP_MODE_IDLE) ;

   load_off() ;

   if (POWER.m_on_run)
      POWER.m_on_run() ;
}

void Power::stop (void)
{
   ATOMIC_BLOCK (ATOMIC_RESTORESTATE)
   {
      if (!m_running)
         return ;

      m_running = false ;

      if (m_on_stop)
         m_on_stop() ;

      battery_off() ;
      load_on() ;

      power_all_disable() ;
      set_sleep_mode (SLEEP_MODE_PWR_DOWN) ;
   }
}

void Power::battery_on (void)
{
   gpio::On::reset() ;
}

void Power::battery_off (void)
{
   gpio::On::set() ;
}

void Power::load_on (void)
{
   gpio::Pwr::reset() ;
}

void Power::load_off (void)
{
   gpio::Pwr::set() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
