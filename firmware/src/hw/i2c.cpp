////////////////////////////////////////////////////////////////////////////////////////////
//
// i2c.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "i2c.h"
#include <avr/io.h>

ISR (TWI_vect)
{
   hw::I2c::_on_twi_event() ;
}

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr uint8_t _ENABLE = _BV(TWEN) | _BV(TWIE) ;

   static constexpr uint8_t START =    _ENABLE | _BV(TWINT) | _BV(TWSTA) ;
   static constexpr uint8_t STOP =     _ENABLE | _BV(TWINT) | _BV(TWSTO) ;
   static constexpr uint8_t MORE =     _ENABLE | _BV(TWINT) ;

   static constexpr uint8_t ACK = _BV(TWEA) ;
}

////////////////////////////////////////////////////////////////////////////////////////////

uint8_t I2c::m_address ;
uint8_t I2c::m_retries ;
I2c::IoBuffer I2c::m_io_buffer ;
uint8_t I2c::m_io_pos ;
volatile uint8_t I2c::m_errno ;
volatile bool I2c::m_sending ;

SINGLETON_IMPL (I2c) ;

I2c::I2c (void)
{
   TWBR = 4 ; // 400KHz at 8MHz clocks
   TWSR = 0 ; // prescaler == 1 (400KHz at 8MHz clocks)
   TWCR = _ENABLE ;
}

I2c::Tx I2c::tx (void)
{
   while (m_sending) ;

   m_io_buffer.clear() ;
   return m_io_buffer ;
}

uint8_t I2c::receive (uint8_t addr, uint8_t sz)
{
   m_address = (addr << 1) | 1 ; // SLA+R
   m_io_buffer.resize (sz) ;

   m_errno = 0xff ;
   TWCR = START ;

   while (m_errno == 0xff) ;
   return m_errno ;
}

uint8_t I2c::send (uint8_t addr)
{
   m_sending = true ;
   m_address = addr << 1 ; // SLA+W
   m_errno = 0xff ;
   TWCR = START ;

   while (m_errno == 0xff) ;
   return m_errno ;
}

void I2c::_complete_request (uint8_t errno)
{
   TWCR = STOP ;
   m_errno = errno ;
   m_sending = false ;
}

inline __attribute__ ((always_inline))
   void I2c::_on_twi_event (void)
   {
      switch (TWSR & ~7)
      {
         case 0x38: // arbitration lost
            TWCR = START ;
            break ;

         case 0x08: // START issued
            TWDR = m_address ;
            TWCR = MORE ;
            break ;

         case 0x18: // SLA+W ACK
            m_retries = 0 ;
            m_io_pos = 0 ;

         case 0x28: // DATA+W ACK
            if (m_io_pos >= m_io_buffer.size())
               _complete_request (0) ;
            else
            {
               TWDR = m_io_buffer[m_io_pos++] ;
               TWCR = MORE ;
            }
            break ;

         case 0x40: // SLA+R ACK
            m_retries = 0 ;
            m_io_pos = 0 ;
            TWCR = MORE | (m_io_pos + 1 < m_io_buffer.size() ? ACK : 0) ;
            break ;

         case 0x50: // DATA+R ACK
            m_io_buffer[m_io_pos++] = TWDR ;
            TWCR = MORE | (m_io_pos < m_io_buffer.size() ? ACK : 0) ;
            break ;

         case 0x58: // DATA+R NACK
            m_io_buffer[m_io_pos] = TWDR ;
            _complete_request (0) ;
            break ;

         case 0x20: // SLA+W NACK
         case 0x48: // SLA+R NACK
            if (++m_retries)
            {
               TWCR = START ;
               break ;
            }

         case 0x30: // DATA+W NACK
         default:
            _complete_request (TWSR & ~7) ;
            break ;
      }
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
