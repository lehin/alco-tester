////////////////////////////////////////////////////////////////////////////////////////////
//
// adc.cpp
//
// Copyright 2014 Alexey Kosilin <kosilin@gmail.com>
//
////////////////////////////////////////////////////////////////////////////////////////////

#include "adc.h"
#include "aux/ll-scheduler.h"
#include "aux/minmax.h"
#include <avr/power.h>

////////////////////////////////////////////////////////////////////////////////////////////

ISR (ADC_vect)
{
   hw::Adc::_on_value_measured_interrupt() ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace
{
   static constexpr int8_t MEASURE_PREPARE = -50 ;
   static constexpr int8_t MEASURE_COUNT = 9 ;
}

////////////////////////////////////////////////////////////////////////////////////////////

namespace hw
{

////////////////////////////////////////////////////////////////////////////////////////////

int8_t Adc::m_measured_count ;
uint16_t Adc::m_measured_sum ;
uint8_t Adc::m_last_admux = (uint8_t)-1 ;
Adc::Pendings Adc::m_pendings ;

SINGLETON_IMPL (Adc) ;

Adc::Adc (void)
{
   ADCSRA = _BV(ADIE) | _BV(ADPS1) | _BV(ADPS2) ;
}

void Adc::measure (uint8_t input, Vref vref, DataSink sink)
{
   bool first = m_pendings.empty() ;
   m_pendings.push_back ({ (uint8_t)(input|vref), sink }) ;

   if (first)
      _check_pendings() ;
}

void Adc::_check_pendings (void)
{
   if (m_pendings.empty())
      return ;

   auto& top = *m_pendings.begin() ;

   m_measured_count = m_last_admux == top.m_admux ? MEASURE_COUNT : MEASURE_PREPARE ;
   m_measured_sum = 0 ;

   ADMUX = m_last_admux = top.m_admux ;
   ADCSRA |= _BV(ADEN) ;
}

void Adc::_on_value_measured (void)
{
   if (!m_measured_count)
      return ;

   if (m_measured_count < 0)
   {
      if (!++m_measured_count)
         m_measured_count = MEASURE_COUNT ;

      return ;
   }

   m_measured_sum += ADC ;

   if (--m_measured_count)
      return ;

   ADCSRA &= ~_BV(ADEN) ;

   if (m_pendings.empty())
      return ;

   m_pendings.begin()->m_sink ((m_measured_sum + MEASURE_COUNT / 2) / MEASURE_COUNT) ;
   m_pendings.pop_front() ;

   _check_pendings() ;
}

inline
   void __attribute__ ((always_inline)) Adc::_on_value_measured_interrupt (void)
   {
      static bool s_event_scheduled ;

      if (s_event_scheduled)
         return ;

      s_event_scheduled = true ;

      aux::LowLatencySched::schedule
         (
            []
            {
               s_event_scheduled = false ;
               _on_value_measured() ;
            }
         ) ;
   }

////////////////////////////////////////////////////////////////////////////////////////////

} // namespace hw
